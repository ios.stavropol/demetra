import React, { useEffect } from 'react';
import {TouchableOpacity, Image} from 'react-native';
import Common from '../utilities/Common';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../styles';

const CloseButton = ({style, navigation}) => {

	return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(32),
			height: Common.getLengthByIPhone7(32),
			alignItems: 'flex-end',
			justifyContent: 'center',
		}, style]}
		onPress={() => {
			navigation.goBack();
		}}>
			<Image
				source={require('./../assets/ic-close.png')}
				style={{
					resizeMode: 'contain',
					width: Common.getLengthByIPhone7(24),
					height: Common.getLengthByIPhone7(24),
					tintColor: colors.BLUE_COLOR,
				}}
			/>
		</TouchableOpacity>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(CloseButton);