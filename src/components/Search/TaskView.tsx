import React, {useEffect, useRef} from 'react';
import {
  Platform,
  ScrollView,
  View,
  Text,
  Animated,
  Easing,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';

const TaskView = ({navigation, style, data}) => {
  const [body, setBody] = React.useState(null);
  const [openView, setOpenView] = React.useState(false);

  const opacity = useRef(new Animated.Value(0));
  const height = useRef(new Animated.Value(0));

  useEffect(() => {
    console.warn(data);
    renderView();
  }, []);

  useEffect(() => {
    console.warn(data);
    renderView();
  }, [data]);

  const renderView = () => {
    let array = [];
    for (let i = 0; i < data?.length; i++) {
      array.push(
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            paddingBottom: Common.getLengthByIPhone7(9),
            paddingTop: Common.getLengthByIPhone7(9),
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomColor: '#e1e1e2',
            borderBottomWidth: i + 1 === data?.length ? 0 : 1,
          }}
          key={i}>
          <Text
            style={{
              width: Common.getLengthByIPhone7(250),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(14),
              marginLeft: Common.getLengthByIPhone7(11),
            }}
            allowFontScaling={false}>
            {data[i].TITLE}
          </Text>
        </View>,
      );
    }
    setBody(array);
  };

  return (
    <Animated.View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
          // height: Common.getLengthByIPhone7(240),
          borderRadius: Common.getLengthByIPhone7(8),
          overflow: 'hidden',
          justifyContent: 'flex-start',
          alignItems: 'center',
          padding: Common.getLengthByIPhone7(20),
          paddingBottom:
            data?.length > 3
              ? Common.getLengthByIPhone7(90)
              : Common.getLengthByIPhone7(20),
          backgroundColor: '#f5f5f6',
          maxHeight: height.current.interpolate({
            inputRange: [0, 1],
            outputRange: [Common.getLengthByIPhone7(240), 100000],
          }),
        },
        style,
      ]}>
      {body}
      {data?.length > 3 ? (
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            height: Common.getLengthByIPhone7(90),
            position: 'absolute',
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <LinearGradient
            colors={['rgba(245, 245, 246, 0)', 'rgba(245, 245, 246, 1)']}
            // start={{x: 0.0, y: 0.5}} end={{x: 1.0, y: 0.5}}
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
              height: Common.getLengthByIPhone7(30),
              position: 'absolute',
              left: 0,
              top: 0,
            }}
          />
          <View
            style={{
              backgroundColor: '#f5f5f6',
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
              height: Common.getLengthByIPhone7(60),
              position: 'absolute',
              left: 0,
              top: Common.getLengthByIPhone7(30),
            }}></View>
          <TouchableOpacity
            style={{
              width: Common.getLengthByIPhone7(300),
              height: Common.getLengthByIPhone7(40),
              borderRadius: Common.getLengthByIPhone7(50),
              borderColor: colors.YELLOW_ACTIVE_COLOR,
              borderWidth: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}
            onPress={() => {
              if (openView) {
                Animated.timing(height.current, {
                  toValue: 0,
                  duration: 300,
                  easing: Easing.linear,
                  useNativeDriver: false, // <-- neccessary
                }).start(() => {
                  Animated.timing(opacity.current, {
                    toValue: 0,
                    duration: 300,
                    easing: Easing.linear,
                    useNativeDriver: false, // <-- neccessary
                  }).start();
                });
              } else {
                Animated.timing(height.current, {
                  toValue: 1,
                  duration: 300,
                  easing: Easing.linear,
                  useNativeDriver: false, // <-- neccessary
                }).start(() => {
                  Animated.timing(opacity.current, {
                    toValue: 1,
                    duration: 300,
                    easing: Easing.linear,
                    useNativeDriver: false,
                  }).start();
                });
              }
              setOpenView(!openView);
            }}>
            <Text
              style={{
                color: colors.YELLOW_ACTIVE_COLOR,
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: '700',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
              }}
              allowFontScaling={false}>
              {openView ? 'Скрыть' : 'Показать больше'}
            </Text>
          </TouchableOpacity>
        </View>
      ) : null}
    </Animated.View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(TaskView);
