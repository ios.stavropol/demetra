import React, {useEffect, useRef} from 'react';
import {
  Platform,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import {config} from '../../constants';

const NewsView = ({data, style, action}) => {
  const [height, setHeight] = React.useState(0);

  useEffect(() => {
    if (data?.FIRST_IMG?.length) {
      Image.getSize(config.BASE_URL + data?.FIRST_IMG, (width, height) => {
        const screenWidth =
          Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40);
        const scaleFactor = width / screenWidth;
        const imageHeight = height / scaleFactor;
        setHeight(imageHeight);
      });
    }
  }, []);

  return (
    <TouchableOpacity
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          borderRadius: Common.getLengthByIPhone7(12),
          paddingBottom: Common.getLengthByIPhone7(20),
          backgroundColor: colors.VIEW_COLOR,
          overflow: 'hidden',
        },
        style,
      ]}
      onPress={() => {
        if (action) {
          action();
        }
      }}>
      {data?.FIRST_IMG?.length ? (
        <Image
          source={{uri: config.BASE_URL + data?.FIRST_IMG}}
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: height,
            resizeMode: 'contain',
          }}
        />
      ) : null}
      <Text
        style={{
          color: colors.GRAY_COLOR,
          fontFamily: 'OpenSans-Regular',
          fontWeight: 'normal',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(10),
          marginTop: Common.getLengthByIPhone7(20),
          marginLeft: Common.getLengthByIPhone7(20),
          marginRight: Common.getLengthByIPhone7(20),
        }}
        numberOfLines={1}
        allowFontScaling={false}>
        {Common.getRusDateTime(new Date(data?.LOG_DATE))}
      </Text>
      <Text
        style={{
          color: colors.TEXT_COLOR,
          fontFamily: 'OpenSans-Bold',
          fontWeight: '700',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(14),
          marginTop: Common.getLengthByIPhone7(15),
          marginLeft: Common.getLengthByIPhone7(20),
          marginRight: Common.getLengthByIPhone7(20),
        }}
        allowFontScaling={false}>
        {data?.TITLE}
      </Text>
      {/* <Text style={{
			color: colors.TEXT_COLOR,
			fontFamily: 'OpenSans-Regular',
			fontWeight: 'normal',
			textAlign: 'left',
			fontSize: Common.getLengthByIPhone7(12),
			marginTop: Common.getLengthByIPhone7(10),
		}}
		numberOfLines={5}
		allowFontScaling={false}>
			{data?.MESSAGE}
		</Text> */}
      {/* <View style={{
			marginTop: Common.getLengthByIPhone7(20),
			height: Common.getLengthByIPhone7(21),
			borderRadius: Common.getLengthByIPhone7(50),
			paddingLeft: Common.getLengthByIPhone7(6),
			paddingRight: Common.getLengthByIPhone7(6),
			backgroundColor: '#eeeeee',
			alignItems: 'center',
			justifyContent: 'center',
		}}>
			<Text style={{
				color: colors.TEXT_COLOR,
				fontFamily: 'OpenSans-Regular',
				fontWeight: 'normal',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(10),
				opacity: 0.7,
			}}
			allowFontScaling={false}>
				#новости компании
			</Text>
		</View> */}
    </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(NewsView);
