import React, {useEffect, useRef} from 'react';
import {
  Platform,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';
import {config} from '../../constants';

const ImportantView = ({data, style, action, readImportant}) => {
  const [news, setNews] = React.useState(null);
  const [height, setHeight] = React.useState(0);

  useEffect(() => {
    setNews(data);
  }, [data]);

  useEffect(() => {
    if (news?.FIRST_IMG?.length) {
      Image.getSize(config.BASE_URL + news?.FIRST_IMG, (width, height) => {
        const screenWidth =
          Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40);
        const scaleFactor = width / screenWidth;
        const imageHeight = height / scaleFactor;
        setHeight(imageHeight);
      });
    }
  }, [news]);

  return (
    <TouchableOpacity
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          borderRadius: Common.getLengthByIPhone7(12),
          overflow: 'hidden',
          justifyContent: 'space-between',
          alignItems: 'flex-start',
          paddingBottom: Common.getLengthByIPhone7(28),
          backgroundColor: '#f7f7f7',
        },
        style,
      ]}
      activeOpacity={0.7}
      onPress={() => {
        if (action) {
          action();
        }
      }}>
      {news?.IS_READ ? (
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            backgroundColor: '#f8f8f8',
          }}
        />
      ) : (
        <LinearGradient
          colors={['#f7f7f7', '#F9F5EB']}
          start={{x: 0.0, y: 0.5}}
          end={{x: 1.0, y: 0.5}}
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: Common.getLengthByIPhone7(176),
            position: 'absolute',
            left: 0,
            top: 0,
          }}
        />
      )}
      {data?.FIRST_IMG?.length ? (
        <Image
          source={{uri: config.BASE_URL + data?.FIRST_IMG}}
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: height,
            resizeMode: 'cover',
          }}
        />
      ) : null}
      <View
        style={{
          width: Common.getLengthByIPhone7(320) - Common.getLengthByIPhone7(40),
          alignItems: 'center',
          justifyContent: 'space-between',
          flexDirection: 'row',
          marginTop: Common.getLengthByIPhone7(20),
          marginLeft: Common.getLengthByIPhone7(20),
          marginRight: Common.getLengthByIPhone7(20),
        }}>
        <Text
          style={{
            color: colors.GRAY_COLOR,
            fontFamily: 'OpenSans-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          numberOfLines={1}
          allowFontScaling={false}>
          {Common.getRusDateTime(new Date(news?.LOG_DATE))}
        </Text>
        <Image
          source={
            news?.IS_READ
              ? require('./../../assets/ic-tick.png')
              : require('./../../assets/ic-info.png')
          }
          style={{
            width: Common.getLengthByIPhone7(24),
            height: Common.getLengthByIPhone7(24),
            resizeMode: 'contain',
          }}
        />
      </View>
      <Text
        style={{
          color: colors.TEXT_COLOR,
          fontFamily: 'OpenSans-Bold',
          fontWeight: 'bold',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(16),
          marginLeft: Common.getLengthByIPhone7(20),
          marginRight: Common.getLengthByIPhone7(20),
        }}
        allowFontScaling={false}>
        {news?.TITLE}
      </Text>
      {news?.IS_READ ? null : (
        <TouchableOpacity
          style={{
            width: Common.getLengthByIPhone7(139),
            height: Common.getLengthByIPhone7(40),
            borderRadius: Common.getLengthByIPhone7(50),
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: colors.YELLOW_ACTIVE_COLOR,
            marginTop: Common.getLengthByIPhone7(20),
            marginLeft: Common.getLengthByIPhone7(20),
          }}
          onPress={() => {
            readImportant(news?.SOURCE_ID)
              .then(() => {
                let dd = JSON.parse(JSON.stringify(news));
                dd.IS_READ = !dd.IS_READ;
                setNews(dd);
              })
              .catch(err => {});
          }}>
          <Text
            style={{
              color: 'white',
              fontFamily: 'OpenSans-SemiBold',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(14),
            }}
            numberOfLines={1}
            allowFontScaling={false}>
            Прочитать
          </Text>
        </TouchableOpacity>
      )}
    </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  readImportant: payload => dispatch.user.readImportant(payload),
});

export default connect(mstp, mdtp)(ImportantView);
