import React, { useEffect, useRef } from 'react';
import {Platform, ScrollView, View, Text, Image, TouchableOpacity} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from '../../utilities/Common';
import { colors } from '../../styles';

const TopButtonView = ({style, data, action}) => {

	const [tab, setTab] = React.useState(0);

	useEffect(() => {
		if (action) {
			action(tab);
		}
	}, [tab]);

	useEffect(() => {
		setTab(data);
	}, [data]);

	return (<TouchableOpacity style={[{
		width: Common.getLengthByIPhone7(0),
		height: Common.getLengthByIPhone7(38),
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: 'white',
		borderBottomColor: colors.BORDER_COLOR,
		borderBottomWidth: 1,
	}, style]}
	activeOpacity={0.7}
	onPress={() => {
		if (action) {
			action();
		}
	}}>
		<TouchableOpacity style={{
			marginLeft: Common.getLengthByIPhone7(20),
			alignItems: 'center',
			justifyContent: 'center',
			borderBottomColor: colors.YELLOW_ACTIVE_COLOR,
			borderBottomWidth: tab === 0 ? 2 : 0,
			height: Common.getLengthByIPhone7(38),
		}}
		activeOpacity={1}
		onPress={() => {
			setTab(0);
		}}>
			<Text style={{
				color: tab === 0 ? colors.TEXT_COLOR : colors.GRAY_COLOR,
				fontFamily: 'OpenSans-SemiBold',
				fontWeight: '600',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(14),
			}}
			numberOfLines={1}
			allowFontScaling={false}>
				Новости
			</Text>
		</TouchableOpacity>
		<TouchableOpacity style={{
			marginLeft: Common.getLengthByIPhone7(20),
			alignItems: 'center',
			justifyContent: 'center',
			borderBottomColor: colors.YELLOW_ACTIVE_COLOR,
			borderBottomWidth: tab === 1 ? 2 : 0,
			height: Common.getLengthByIPhone7(38),
		}}
		activeOpacity={1}
		onPress={() => {
			setTab(1);
		}}>
			<Text style={{
				color: tab === 1 ? colors.TEXT_COLOR : colors.GRAY_COLOR,
				fontFamily: 'OpenSans-SemiBold',
				fontWeight: '600',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(14),
			}}
			numberOfLines={1}
			allowFontScaling={false}>
				Важное сообщение
			</Text>
		</TouchableOpacity>
	</TouchableOpacity>);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
    
});

export default connect(mstp, mdtp)(TopButtonView);