import React, {useEffect} from 'react';
import {View, ActivityIndicator} from 'react-native';
import {colors} from '../styles';
import Common from './../utilities/Common';
import {RootState, Dispatch} from './../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';

const LoadingView = ({endLoading, isRequestGoing}) => {
  return (
    <Spinner
      visible={isRequestGoing}
      // textContent={'Loading...'}
      customIndicator={
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 20000,
          }}>
          <ActivityIndicator size={'large'} color={'#F9B441'} />
        </View>
      }
      overlayColor={'rgba(35, 22, 16, 0.6)'}
      textStyle={{color: '#FFF'}}
    />
  );
};

const mstp = (state: RootState) => ({
  isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
  endLoading: () => dispatch.user.endLoading(),
});

export default connect(mstp, mdtp)(LoadingView);
