import React, {useEffect, useRef} from 'react';
import {TouchableOpacity, Image} from 'react-native';
import Common from '../utilities/Common';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import {colors} from './../styles';
import DropdownAlert from 'react-native-dropdownalert';

const AlertView = ({style, setShowAlert, navigation}) => {
  let dropDownAlertRef = useRef();

  useEffect(() => {
    setShowAlert(_fetchData);
  }, []);

  const _fetchData = async () => {
    console.warn('_fetchData');
    dropDownAlertRef.alertWithType(
      'error',
      'Ошибка',
      'Время сессии истекло. Пожалуйста, авторизируйтесь повторно',
    );
  };

  return (
    <DropdownAlert
      ref={ref => {
        if (ref) {
          dropDownAlertRef = ref;
        }
      }}
    />
  );
};

const mstp = (state: RootState) => ({
  isRequestGoing: state.user.isRequestGoing,
});

const mdtp = (dispatch: Dispatch) => ({
  setShowAlert: payload => dispatch.user.setShowAlert(payload),
});

export default connect(mstp, mdtp)(AlertView);
