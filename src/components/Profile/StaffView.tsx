import React, {useEffect, useRef} from 'react';
import {
  Platform,
  Animated,
  Easing,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';
import {config} from '../../constants';

const StaffView = ({style, navigation, data, from}) => {
  const [openView, setOpenView] = React.useState(false);
  const opacity = useRef(new Animated.Value(0));
  const height = useRef(new Animated.Value(0));
  const [subordinate, setSubordinate] = React.useState(null);
  const [funcHead, setFuncHead] = React.useState(null);
  const [managers, setManagers] = React.useState(null);

  useEffect(() => {
    console.warn('data: ', from);
    if (data?.subordinate?.length) {
      let array = [];
      array.push(
        <Text
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            marginTop: Common.getLengthByIPhone7(17),
            color: colors.GRAY_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          allowFontScaling={false}>
          Подчиненные
        </Text>,
      );

      for (let i = 0; i < data?.subordinate?.length; i++) {
        array.push(
          renderUser(
            data?.subordinate[i],
            {
              marginTop: Common.getLengthByIPhone7(8),
              marginBottom:
                i + 1 === data?.subordinate?.length
                  ? Common.getLengthByIPhone7(8)
                  : 0,
            },
            obj => {
              console.warn(from, ', ', obj);
              if (from === 'main') {
                navigation.push('AlienProfile3', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else if (from === 'AlienProfile3') {
                navigation.push('AlienProfile3', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else if (from === 'AlienProfile2') {
                navigation.push('AlienProfile2', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else if (from === 'AlienProfile4') {
                navigation.push('AlienProfile4', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else if (from === 'AlienProfile5') {
                navigation.push('AlienProfile5', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else {
                navigation.push('AlienProfile', {
                  data: {ID: obj.ID},
                  from: from,
                });
              }
            },
          ),
        );
      }
      setSubordinate(array);
    }

    if (
      data?.personal_func_head &&
      Object.keys(data?.personal_func_head).length
    ) {
      let array = [];
      console.warn('data?.personal_func_head: ', data?.personal_func_head);
      array.push(
        <Text
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            marginTop: Common.getLengthByIPhone7(17),
            color: colors.GRAY_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          allowFontScaling={false}>
          Функциональный руководитель
        </Text>,
      );

      array.push(
        renderUser(
          data?.personal_func_head,
          {
            marginTop: Common.getLengthByIPhone7(8),
            marginBottom: Common.getLengthByIPhone7(8),
          },
          obj => {
            if (from === 'main') {
              navigation.push('AlienProfile3', {
                data: {ID: obj.ID},
                from: from,
              });
            } else if (from === 'AlienProfile3') {
              navigation.push('AlienProfile3', {
                data: {ID: obj.ID},
                from: from,
              });
            } else if (from === 'AlienProfile2') {
              navigation.push('AlienProfile2', {
                data: {ID: obj.ID},
                from: from,
              });
            } else if (from === 'AlienProfile4') {
              navigation.push('AlienProfile4', {
                data: {ID: obj.ID},
                from: from,
              });
            } else if (from === 'AlienProfile5') {
              navigation.push('AlienProfile5', {
                data: {ID: obj.ID},
                from: from,
              });
            } else {
              navigation.push('AlienProfile', {data: {ID: obj.ID}, from: from});
            }
          },
        ),
      );
      setFuncHead(array);
    }

    if (data?.managers && Object.keys(data?.managers).length) {
      let array = [];
      array.push(
        <Text
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            marginTop: Common.getLengthByIPhone7(17),
            color: colors.GRAY_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          allowFontScaling={false}>
          Административный руководитель
        </Text>,
      );

      let keys = Object.keys(data?.managers);
      for (let i = 0; i < keys.length; i++) {
        array.push(
          renderUser(
            data?.managers[keys[i]],
            {
              marginTop: Common.getLengthByIPhone7(8),
              marginBottom: Common.getLengthByIPhone7(8),
            },
            obj => {
              console.warn(from);
              if (from === 'main') {
                navigation.push('AlienProfile3', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else if (from === 'AlienProfile3') {
                navigation.push('AlienProfile3', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else if (from === 'AlienProfile2') {
                navigation.push('AlienProfile2', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else if (from === 'AlienProfile4') {
                navigation.push('AlienProfile4', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else if (from === 'AlienProfile5') {
                navigation.push('AlienProfile5', {
                  data: {ID: obj.ID},
                  from: from,
                });
              } else {
                navigation.push('AlienProfile', {
                  data: {ID: obj.ID},
                  from: from,
                });
              }
            },
          ),
        );
      }

      setManagers(array);
    }
  }, []);

  const renderUser = (obj, style, action) => {
    return (
      <TouchableOpacity
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: Common.getLengthByIPhone7(40),
            borderRadius: Common.getLengthByIPhone7(15),
            padding: Common.getLengthByIPhone7(10),
            borderColor: colors.BORDER_COLOR,
            borderWidth: 1,
            flexDirection: 'row',
            alignItems: 'center',
          },
          style,
        ]}
        onPress={() => {
          if (action) {
            action(obj);
          }
        }}>
        <View
          style={{
            width: Common.getLengthByIPhone7(40),
            height: Common.getLengthByIPhone7(40),
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: Common.getLengthByIPhone7(20),
            overflow: 'hidden',
            borderWidth: 2,
            borderColor: 'rgba(248, 170, 43, 0.3)',
            backgroundColor: 'white',
          }}>
          {obj?.PERSONAL_PHOTO?.FILE?.SRC?.length ? (
            <Image
              source={{uri: config.BASE_URL + obj?.PERSONAL_PHOTO?.FILE?.SRC}}
              style={{
                width: Common.getLengthByIPhone7(40),
                height: Common.getLengthByIPhone7(40),
                resizeMode: 'cover',
              }}
            />
          ) : (
            <Image
              source={require('./../../assets/ic-placeholder-user.png')}
              style={{
                width: Common.getLengthByIPhone7(20),
                height: Common.getLengthByIPhone7(20),
              }}
            />
          )}
        </View>
        <View
          style={{
            marginLeft: Common.getLengthByIPhone7(10),
            width:
              Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(112),
          }}>
          <Text
            style={{
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
            }}
            allowFontScaling={false}>
            {obj.FULL_NAME}
          </Text>
          {obj?.user_work_position ? (
            <Text
              style={{
                marginTop: 2,
                color: colors.TEXT_COLOR,
                fontFamily: 'OpenSans-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(10),
              }}
              allowFontScaling={false}>
              {obj?.user_work_position}
            </Text>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  };

  const renderView = (title, value, style) => {
    return (
      <View
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            height: Common.getLengthByIPhone7(63),
            borderBottomColor: 'rgba(117, 77, 58, 0.1)',
            borderBottomWidth: 1,
            justifyContent: 'center',
          },
          style,
        ]}>
        <Text
          style={{
            color: colors.GRAY_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          allowFontScaling={false}>
          {title}
        </Text>
        <Text
          style={{
            marginTop: 2,
            color: colors.TEXT_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(14),
          }}
          allowFontScaling={false}>
          {value}
        </Text>
      </View>
    );
  };

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
          minHeight: Common.getLengthByIPhone7(75),
          borderRadius: Common.getLengthByIPhone7(15),
          backgroundColor: 'rgba(232, 212, 204, 0.3)',
          alignItems: 'center',
          overflow: 'hidden',
        },
        style,
      ]}>
      <TouchableOpacity
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
          minHeight: Common.getLengthByIPhone7(75),
          borderBottomColor: 'rgba(117, 77, 58, 0.1)',
          borderBottomWidth: 1,
          alignItems: 'flex-start',
          justifyContent: 'center',
          paddingTop: Common.getLengthByIPhone7(10),
          paddingBottom: Common.getLengthByIPhone7(10),
        }}
        activeOpacity={1}
        onPress={() => {
          if (openView) {
            Animated.timing(height.current, {
              toValue: 0,
              duration: 300,
              easing: Easing.linear,
              useNativeDriver: false,
            }).start(() => {
              Animated.timing(opacity.current, {
                toValue: 0,
                duration: 300,
                easing: Easing.linear,
                useNativeDriver: false,
              }).start();
            });
          } else {
            Animated.timing(height.current, {
              toValue: 1,
              duration: 300,
              easing: Easing.linear,
              useNativeDriver: false,
            }).start(() => {
              Animated.timing(opacity.current, {
                toValue: 1,
                duration: 300,
                easing: Easing.linear,
                useNativeDriver: false,
              }).start();
            });
          }
          setOpenView(!openView);
        }}>
        <Text
          style={{
            color: colors.TEXT_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(14),
          }}
          allowFontScaling={false}>
          {data?.user_work_position}
        </Text>
        <Text
          style={{
            marginTop: 5,
            color: colors.TEXT_COLOR,
            fontFamily: 'OpenSans-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(12),
          }}
          allowFontScaling={false}>
          {data?.TITLE}
        </Text>
        <Animated.Image
          source={require('./../../assets/ic-arrow-down.png')}
          style={{
            width: Common.getLengthByIPhone7(12),
            height: Common.getLengthByIPhone7(6),
            position: 'absolute',
            right: 0,
            tintColor: 'rgba(117, 77, 58, 0.9)',
            transform: [
              {
                rotate: height.current.interpolate({
                  inputRange: [0, 1],
                  outputRange: ['0deg', '180deg'],
                }),
              },
            ],
          }}
        />
      </TouchableOpacity>
      <Animated.View
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
          alignItems: 'center',
          // opacity: opacity.current,
          maxHeight: height.current.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 10000],
          }),
        }}>
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            alignItems: 'center',
          }}>
          {renderView('Организация', data?.TITLE, {})}
          {renderView('Отдел', data?.user_func_department, {})}
          {renderView('Место работы', data?.user_work_place, {})}
          {renderView('Табельный номер', data?.user_tab_number, {})}
          {/* {renderView('Корпоративный ID', '654120881', {

				})} */}
          {managers}
          {funcHead}
          {subordinate}
        </View>
      </Animated.View>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(StaffView);
