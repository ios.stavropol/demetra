import React, {useEffect, useRef} from 'react';
import {
  Platform,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import {config} from '../../constants';

const UserView = ({style, chief, index, data, onClick}) => {
  const [staff, setStaff] = React.useState('');

  useEffect(() => {
    let str = '';
    if (data?.LEGAL_INFO) {
      let keys = Object.keys(data?.LEGAL_INFO);
      for (let i = 0; i < keys.length; i++) {
        str =
          data?.LEGAL_INFO[keys[i]]?.user_work_position +
          ' (' +
          data?.LEGAL_INFO[keys[i]].TITLE +
          ')';
        break;
      }
    }
    setStaff(str);
  }, []);

  useEffect(() => {
    let str = '';
    if (data?.LEGAL_INFO) {
      let keys = Object.keys(data?.LEGAL_INFO);
      for (let i = 0; i < keys.length; i++) {
        str =
          data?.LEGAL_INFO[keys[i]]?.user_work_position +
          ' (' +
          data?.LEGAL_INFO[keys[i]].TITLE +
          ')';
        break;
      }
    }
    setStaff(str);
  }, [data]);

  return (
    <TouchableOpacity
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          minHeight: Common.getLengthByIPhone7(74),
          borderRadius: Common.getLengthByIPhone7(15),
          padding: Common.getLengthByIPhone7(10),
          paddingLeft: Common.getLengthByIPhone7(15),
          paddingRight: Common.getLengthByIPhone7(15),
          borderColor:
            data?.ID === chief?.ID
              ? colors.YELLOW_ACTIVE_COLOR
              : colors.BORDER_COLOR,
          borderWidth: 1,
          flexDirection: 'row',
          alignItems: 'center',
        },
        style,
      ]}
      key={index}
      onPress={() => {
        if (onClick) {
          onClick();
        }
      }}>
      {/* <View
        style={{
          width: Common.getLengthByIPhone7(40),
          height: Common.getLengthByIPhone7(40),
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: Common.getLengthByIPhone7(20),
          overflow: 'hidden',
          borderWidth: 2,
          borderColor: 'rgba(248, 170, 43, 0.3)',
        }}>
        {data?.PERSONAL_PHOTO?.FILE?.SRC?.length ? (
          <Image
            source={{uri: config.BASE_URL + data?.PERSONAL_PHOTO?.FILE?.SRC}}
            style={{
              width: Common.getLengthByIPhone7(40),
              height: Common.getLengthByIPhone7(40),
              resizeMode: 'cover',
            }}
          />
        ) : (
          <Image
            source={require('./../../assets/ic-placeholder-user.png')}
            style={{
              width: Common.getLengthByIPhone7(20),
              height: Common.getLengthByIPhone7(20),
            }}
          />
        )}
      </View> */}
      <View
        style={{
          // marginLeft: Common.getLengthByIPhone7(12),
          // width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(112),
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
        }}>
        <Text
          style={{
            color: colors.TEXT_COLOR,
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(12),
          }}
          allowFontScaling={false}>
          {data?.FULL_NAME}
        </Text>
        {staff?.length ? (
          <Text
            style={{
              marginTop: 5,
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(10),
            }}
            allowFontScaling={false}>
            {staff}
          </Text>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(UserView);
