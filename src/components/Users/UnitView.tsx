import React, {useEffect, useRef} from 'react';
import {
  Platform,
  Animated,
  Easing,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import {config} from '../../constants';
import FastImage from 'react-native-fast-image';

const UnitView = ({style, first, data, onChiefClick, onUnitClick}) => {
  const [body, setBody] = React.useState(null);
  const [staff, setStaff] = React.useState('');
  const [openView, setOpenView] = React.useState(false);
  const [image, setImage] = React.useState(null);

  const opacity = useRef(new Animated.Value(0));
  const height = useRef(new Animated.Value(0));

  useEffect(() => {
    if (data) {
      let keys = Object.keys(data?.SUB_DEPARTMENTS);
      let array = [];
      for (let i = 0; i < keys.length; i++) {
        array.push(
          <UnitView
            style={{
              marginTop: Common.getLengthByIPhone7(10),
            }}
            onChiefClick={onChiefClick}
            onUnitClick={onUnitClick}
            data={data?.SUB_DEPARTMENTS[keys[i]]}
          />,
        );
      }
      setBody(array);
      if (data?.UF_HEAD) {
        let str = data?.UF_HEAD?.user_work_position;
        setStaff(str);
      }
      if (first) {
        Animated.timing(height.current, {
          toValue: 1,
          duration: 300,
          easing: Easing.linear,
          useNativeDriver: false,
        }).start(() => {
          Animated.timing(opacity.current, {
            toValue: 1,
            duration: 300,
            easing: Easing.linear,
            useNativeDriver: false,
          }).start();
        });
        setOpenView(true);
      }
    }
  }, []);

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
          alignItems: 'flex-end',
        },
        style,
      ]}
      key={data?.ID}>
      <View
        style={{
          width:
            Common.getLengthByIPhone7(0) -
            Common.getLengthByIPhone7(20) -
            Common.getLengthByIPhone7(10) * (data?.DEPTH_LEVEL - 1),
          borderRadius: Common.getLengthByIPhone7(12),
          backgroundColor: '#f5f5f6',
          alignItems: 'center',
        }}>
        <TouchableOpacity
          style={{
            width:
              Common.getLengthByIPhone7(0) -
              Common.getLengthByIPhone7(60) -
              Common.getLengthByIPhone7(10) * (data?.DEPTH_LEVEL - 1),
            paddingTop: Common.getLengthByIPhone7(15),
            paddingBottom: Common.getLengthByIPhone7(15),
            borderBottomColor: 'rgba(196, 196, 196, 0.4)',
            borderBottomWidth: data?.UF_HEAD ? 1 : 0,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
          onPress={() => {
            if (onUnitClick && data?.USER_COUNT > 0) {
              onUnitClick(data);
            }
          }}>
          <Text
            style={{
              width:
                Common.getLengthByIPhone7(0) -
                Common.getLengthByIPhone7(110) -
                Common.getLengthByIPhone7(10) * (data?.DEPTH_LEVEL - 1),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: '700',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
            }}
            allowFontScaling={false}>
            {data?.NAME}
          </Text>
          {data?.USER_COUNT > 0 ? (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <View
                style={{
                  minWidth: Common.getLengthByIPhone7(24),
                  height: Common.getLengthByIPhone7(24),
                  borderRadius: Common.getLengthByIPhone7(12),
                  paddingLeft: Common.getLengthByIPhone7(9),
                  paddingRight: Common.getLengthByIPhone7(9),
                  // backgroundColor: colors.YELLOW_ACTIVE_COLOR,
                  backgroundColor: 'white',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: colors.TEXT_COLOR,
                    fontFamily: 'OpenSans-SemiBold',
                    fontWeight: '600',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(10),
                  }}
                  allowFontScaling={false}>
                  {data?.USER_COUNT}
                </Text>
              </View>
              <Image
                source={require('./../../assets/ic-arrow-right.png')}
                style={{
                  width: Common.getLengthByIPhone7(6),
                  height: Common.getLengthByIPhone7(12),
                  marginLeft: Common.getLengthByIPhone7(5),
                  tintColor: '#c4c4c4',
                }}
              />
            </View>
          ) : null}
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width:
              Common.getLengthByIPhone7(0) -
              Common.getLengthByIPhone7(60) -
              Common.getLengthByIPhone7(10) * (data?.DEPTH_LEVEL - 1),
            paddingTop: data?.UF_HEAD ? Common.getLengthByIPhone7(15) : 0,
            paddingBottom: data?.UF_HEAD ? Common.getLengthByIPhone7(15) : 0,
            flexDirection: 'row',
            alignItems: 'center',
            maxHeight: data?.UF_HEAD ? 10000 : 0,
            opacity: data?.UF_HEAD ? 1 : 0,
          }}
          onPress={() => {
            console.warn(data?.UF_HEAD);
            if (onChiefClick && data?.UF_HEAD) {
              onChiefClick(data?.UF_HEAD);
            }
          }}>
          <View
            style={{
              width: Common.getLengthByIPhone7(40),
              height: Common.getLengthByIPhone7(40),
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: Common.getLengthByIPhone7(20),
              overflow: 'hidden',
              borderWidth: 2,
              borderColor: 'rgba(248, 170, 43, 0.3)',
              backgroundColor: 'white',
            }}>
            {data?.UF_HEAD?.PERSONAL_PHOTO?.FILE?.SRC?.length ? (
              <FastImage
                source={{
                  uri:
                    config.BASE_URL + data?.UF_HEAD?.PERSONAL_PHOTO?.FILE?.SRC,
                }}
                style={{
                  width: Common.getLengthByIPhone7(40),
                  height: Common.getLengthByIPhone7(40),
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
            ) : (
              <Image
                source={require('./../../assets/ic-placeholder-user.png')}
                style={{
                  width: Common.getLengthByIPhone7(20),
                  height: Common.getLengthByIPhone7(20),
                }}
              />
            )}
          </View>
          <View
            style={{
              marginLeft: Common.getLengthByIPhone7(10),
            }}>
            <Text
              style={{
                width:
                  Common.getLengthByIPhone7(0) -
                  Common.getLengthByIPhone7(110) -
                  Common.getLengthByIPhone7(10) * (data?.DEPTH_LEVEL - 1),
                color: colors.TEXT_COLOR,
                fontFamily: 'OpenSans-Bold',
                fontWeight: '700',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(12),
              }}
              allowFontScaling={false}>
              {data?.UF_HEAD?.FULL_NAME}
            </Text>
            <Text
              style={{
                width:
                  Common.getLengthByIPhone7(0) -
                  Common.getLengthByIPhone7(110) -
                  Common.getLengthByIPhone7(10) * (data?.DEPTH_LEVEL - 1),
                color: colors.TEXT_COLOR,
                fontFamily: 'OpenSans-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(10),
              }}
              allowFontScaling={false}>
              {staff}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      {Object.keys(data?.SUB_DEPARTMENTS).length ? (
        <View
          style={{
            width:
              Common.getLengthByIPhone7(0) -
              Common.getLengthByIPhone7(20) -
              Common.getLengthByIPhone7(10) * (data?.DEPTH_LEVEL - 1),
          }}>
          <TouchableOpacity
            style={{
              marginTop: -Common.getLengthByIPhone7(12),
              marginLeft: Common.getLengthByIPhone7(10),
              width: Common.getLengthByIPhone7(24),
              height: Common.getLengthByIPhone7(24),
            }}
            onPress={() => {
              if (openView) {
                Animated.timing(height.current, {
                  toValue: 0,
                  duration: 300,
                  easing: Easing.linear,
                  useNativeDriver: false,
                }).start(() => {
                  Animated.timing(opacity.current, {
                    toValue: 0,
                    duration: 300,
                    easing: Easing.linear,
                    useNativeDriver: false,
                  }).start();
                });
              } else {
                Animated.timing(height.current, {
                  toValue: 1,
                  duration: 300,
                  easing: Easing.linear,
                  useNativeDriver: false,
                }).start(() => {
                  Animated.timing(opacity.current, {
                    toValue: 1,
                    duration: 300,
                    easing: Easing.linear,
                    useNativeDriver: false,
                  }).start();
                });
              }
              setOpenView(!openView);
            }}>
            <Image
              source={
                openView
                  ? require('./../../assets/ic-minus.png')
                  : require('./../../assets/ic-plus.png')
              }
              style={{
                width: Common.getLengthByIPhone7(24),
                height: Common.getLengthByIPhone7(24),
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        </View>
      ) : null}
      <Animated.View
        style={{
          maxHeight: height.current.interpolate({
            inputRange: [0, 1],
            outputRange: [0, 100000],
          }),
          overflow: 'hidden',
        }}>
        {body}
      </Animated.View>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(UnitView);
