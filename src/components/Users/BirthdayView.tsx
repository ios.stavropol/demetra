import React, {useEffect, useRef} from 'react';
import {
  Platform,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';

let days = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];

const BirthdayView = ({style, index, data, onClick}) => {
  const [staff, setStaff] = React.useState(null);
  const [day, setDay] = React.useState('');
  const [dayStr, setDayStr] = React.useState('');

  useEffect(() => {
    let str = [];
    if (data?.LEGAL_INFO) {
      let keys = Object.keys(data?.LEGAL_INFO);
      for (let i = 0; i < Math.min(2, keys.length); i++) {
        str.push(
          <Text
            style={{
              marginTop: 5,
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(10),
            }}
            key={i}
            allowFontScaling={false}>
            {data?.LEGAL_INFO[keys[i]]?.user_work_position} (
            {data?.LEGAL_INFO[keys[i]]?.TITLE})
          </Text>,
        );
      }
    }
    let dd = data?.PERSONAL_BIRTHDAY;
    dd = dd.split('.');

    let currentYear = new Date().getFullYear();
    let birthdayDate = new Date(currentYear, dd[1] - 1, dd[0], 23, 59, 59, 59);
    // let birthdayDate2 = new Date(currentYear, dd[1] - 1, dd[0]);
    var now = new Date().valueOf();
    if (birthdayDate.valueOf() < now) {
      console.warn(
        'birthdayDate.valueOf(): ',
        birthdayDate.valueOf(),
        ' now: ',
        now,
      );
      birthdayDate.setFullYear(currentYear + 1);
    }
    setDayStr(days[birthdayDate.getDay()]);
    dd = dd[0] + '.' + dd[1];
    setDay(dd);
    setStaff(str);
  }, []);

  useEffect(() => {
    let str = [];
    if (data?.LEGAL_INFO) {
      let keys = Object.keys(data?.LEGAL_INFO);
      for (let i = 0; i < Math.min(2, keys.length); i++) {
        str.push(
          <Text
            style={{
              marginTop: 5,
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(10),
            }}
            key={i}
            allowFontScaling={false}>
            {data?.LEGAL_INFO[keys[i]]?.user_work_position} (
            {data?.LEGAL_INFO[keys[i]]?.TITLE})
          </Text>,
        );
      }
    }
    let dd = data?.PERSONAL_BIRTHDAY;
    dd = dd.split('.');

    let currentYear = new Date().getFullYear();
    let birthdayDate = new Date(currentYear, dd[1] - 1, dd[0], 23, 59, 59, 59);
    var now = new Date().valueOf();
    if (birthdayDate.valueOf() < now) {
      birthdayDate.setFullYear(currentYear + 1);
    }
    setDayStr(days[birthdayDate.getDay()]);
    dd = dd[0] + '.' + dd[1];
    setDay(dd);
    setStaff(str);
  }, [data]);

  return (
    <TouchableOpacity
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
          minHeight: Common.getLengthByIPhone7(74),
          borderRadius: Common.getLengthByIPhone7(15),
          padding: Common.getLengthByIPhone7(10),
          borderColor: colors.BORDER_COLOR,
          borderWidth: 1,
          flexDirection: 'row',
          alignItems: 'center',
        },
        style,
      ]}
      key={data?.ID}
      onPress={() => {
        if (onClick) {
          onClick();
        }
      }}>
      <View
        style={{
          width: Common.getLengthByIPhone7(44),
          height: Common.getLengthByIPhone7(54),
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#F9F5EB',
          borderRadius: Common.getLengthByIPhone7(6),
        }}>
        <Text
          style={{
            color: 'black',
            opacity: 0.25,
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          allowFontScaling={false}>
          {dayStr}
        </Text>
        <View
          style={{
            marginTop: 2,
            width: Common.getLengthByIPhone7(38),
            height: Common.getLengthByIPhone7(30),
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'white',
            borderRadius: Common.getLengthByIPhone7(4),
          }}>
          <Text
            style={{
              color: colors.YELLOW_ACTIVE_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
            }}
            allowFontScaling={false}>
            {day}
          </Text>
        </View>
      </View>
      <View
        style={{
          marginLeft: Common.getLengthByIPhone7(8),
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(112),
        }}>
        <Text
          style={{
            color: colors.TEXT_COLOR,
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(12),
          }}
          allowFontScaling={false}>
          {data?.FULL_NAME}
        </Text>
        {staff}
      </View>
    </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(BirthdayView);
