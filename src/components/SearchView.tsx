import React, {useCallback, forwardRef, useEffect, useRef} from 'react';
import {
  Image,
  TextInput,
  View,
  TouchableOpacity,
  Linking,
  Text,
  Alert,
} from 'react-native';
import {RootState, Dispatch} from './../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Common from '../utilities/Common';
import {colors, typography} from './../styles';

const SearchView = forwardRef(
  ({onSearch, value, onSubmit, auto, style}, ref) => {
    const [text, setText] = React.useState('');

    useEffect(() => {
      setText(value);
    }, [value]);

    useEffect(() => {
      if (onSearch) {
        onSearch(text);
      }
    }, [text]);

    useEffect(() => {}, []);

    const focus = () => {
      console.warn('focus');
    };

    return (
      <View
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            height: Common.getLengthByIPhone7(40),
            borderRadius: Common.getLengthByIPhone7(10),
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: 'rgba(242, 242, 245, 0.44)',
          },
          style,
        ]}>
        <TextInput
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            height: Common.getLengthByIPhone7(40),
            borderRadius: Common.getLengthByIPhone7(10),
            color: colors.TEXT_COLOR,
            fontSize: Common.getLengthByIPhone7(14),
            fontFamily: 'OpenSans-Regular',
            fontWeight: '400',
            textAlign: 'left',
            paddingLeft: Common.getLengthByIPhone7(49),
            paddingRight: Common.getLengthByIPhone7(15),
            position: 'absolute',
            left: 0,
            top: 0,
          }}
          ref={ref}
          placeholderTextColor={colors.GRAY_COLOR}
          placeholder={'Поиск'}
          returnKeyType={'done'}
          allowFontScaling={false}
          underlineColorAndroid={'transparent'}
          onSubmitEditing={() => {
            if (onSubmit) {
              onSubmit(text);
            }
          }}
          onFocus={() => {}}
          onBlur={() => {}}
          onChangeText={code => {
            setText(code);
          }}
          value={text}
        />
        <Image
          source={require('./../assets/ic-search.png')}
          style={{
            width: Common.getLengthByIPhone7(24),
            height: Common.getLengthByIPhone7(24),
            resizeMode: 'contain',
            position: 'absolute',
            left: Common.getLengthByIPhone7(10),
          }}
        />
        {text?.length ? (
          <TouchableOpacity
            style={{
              position: 'absolute',
              right: Common.getLengthByIPhone7(10),
            }}
            onPress={() => {
              setText('');
              if (onSubmit) {
                onSubmit('');
              }
              ref?.current?.focus();
            }}>
            <Image
              source={require('./../assets/ic-cancel.png')}
              style={{
                width: Common.getLengthByIPhone7(18),
                height: Common.getLengthByIPhone7(18),
                resizeMode: 'contain',
              }}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    );
  },
);

export default SearchView;
