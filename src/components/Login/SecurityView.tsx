import React, {useEffect, useRef} from 'react';
import {Image, TextInput, View, Text, TouchableOpacity} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';

const SecurityView = ({
  title,
  value,
  textContentType,
  style,
  keyboardType,
  onChange,
  bold,
}) => {
  const [text, setText] = React.useState('');
  const [security, setSecurity] = React.useState(true);

  useEffect(() => {
    if (onChange) {
      onChange(text);
    }
  }, [text]);

  useEffect(() => {
    setText(value);
  }, [value]);

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
        },
        style,
      ]}>
      <Text
        style={{
          color: bold ? colors.TEXT_COLOR : colors.GRAY_COLOR,
          fontFamily: 'OpenSans-SemiBold',
          fontWeight: bold ? 'bold' : '500',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(12),
        }}
        numberOfLines={1}
        allowFontScaling={false}>
        {title}
      </Text>
      <View
        style={{
          justifyContent: 'center',
          marginTop: Common.getLengthByIPhone7(7),
        }}>
        <TextInput
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            height: Common.getLengthByIPhone7(40),
            borderRadius: Common.getLengthByIPhone7(8),
            borderColor: colors.BORDER_COLOR,
            borderWidth: 1,
            color: colors.TEXT_COLOR,
            fontSize: Common.getLengthByIPhone7(14),
            fontFamily: 'OpenSans-Regular',
            fontWeight: '400',
            textAlign: 'left',
            paddingLeft: Common.getLengthByIPhone7(15),
            paddingRight: Common.getLengthByIPhone7(15),
          }}
          textContentType={textContentType}
          secureTextEntry={security}
          returnKeyType={'done'}
          keyboardType={keyboardType}
          allowFontScaling={false}
          underlineColorAndroid={'transparent'}
          onSubmitEditing={() => {}}
          onFocus={() => {}}
          onBlur={() => {}}
          onChangeText={code => {
            setText(code);
          }}
          value={text}
        />
        <TouchableOpacity
          style={{
            position: 'absolute',
            right: Common.getLengthByIPhone7(16),
            height: Common.getLengthByIPhone7(40),
            alignItems: 'center',
            justifyContent: 'center',
          }}
          onPress={() => {
            setSecurity(!security);
          }}>
          <Image
            source={
              security
                ? require('./../../assets/ic-eye.png')
                : require('./../../assets/ic-eye-off.png')
            }
            style={{
              width: Common.getLengthByIPhone7(24),
              height: Common.getLengthByIPhone7(24),
              resizeMode: 'contain',
            }}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(SecurityView);
