import React, { useCallback, useEffect, useRef } from 'react';
import { Image, StatusBar, View, TouchableOpacity, Linking, Text, Alert } from 'react-native';
import { RootState, Dispatch } from './../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Common from '../utilities/Common';
import { colors, typography } from './../styles';

const YellowButton = ({title, disabled, onPress, style}) => {

	const [button, setButton] = React.useState(<TouchableOpacity style={[{
		width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
		height: Common.getLengthByIPhone7(40),
		borderRadius: Common.getLengthByIPhone7(50),
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.YELLOW_ACTIVE_COLOR,
		opacity: 0.4,
	}, style]}
	onPress={() => {
		if (onPress) {
			onPress();
		}
	}}>
		<Text style={[{
			fontFamily: 'OpenSans-SemiBold',
			fontSize: Common.getLengthByIPhone7(14),
			color: 'white',
		}]}
		allowFontScaling={false}>
			{title}
		</Text>
	</TouchableOpacity>);

	useEffect(() => {
		if (disabled) {
			setButton(<TouchableOpacity style={[{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
				height: Common.getLengthByIPhone7(40),
				borderRadius: Common.getLengthByIPhone7(50),
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: colors.YELLOW_ACTIVE_COLOR,
				opacity: 0.4,
			}, style]}
			onPress={() => {
				if (onPress) {
					onPress();
				}
			}}>
				<Text style={[{
					fontFamily: 'OpenSans-SemiBold',
					fontSize: Common.getLengthByIPhone7(14),
					color: 'white',
				}]}
				allowFontScaling={false}>
					{title}
				</Text>
			</TouchableOpacity>);
		} else {
			setButton(<TouchableOpacity style={[{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
				height: Common.getLengthByIPhone7(40),
				borderRadius: Common.getLengthByIPhone7(50),
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: colors.YELLOW_ACTIVE_COLOR,
			}, style]}
			onPress={() => {
				if (onPress) {
					onPress();
				}
			}}>
				<Text style={[{
					fontFamily: 'OpenSans-SemiBold',
					fontSize: Common.getLengthByIPhone7(14),
					color: 'white',
				}]}
				allowFontScaling={false}>
					{title}
				</Text>
			</TouchableOpacity>);
		}
	}, [disabled]);

	return (<TouchableOpacity style={[{
		width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
		height: Common.getLengthByIPhone7(40),
		borderRadius: Common.getLengthByIPhone7(50),
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: colors.YELLOW_ACTIVE_COLOR,
	}, style]}
	onPress={() => {
		if (onPress) {
			onPress();
		}
	}}>
		<Text style={[{
			fontFamily: 'OpenSans-SemiBold',
			fontSize: Common.getLengthByIPhone7(14),
			color: 'white',
		}]}
		allowFontScaling={false}>
			{title}
		</Text>
	</TouchableOpacity>);
};

export default YellowButton;
