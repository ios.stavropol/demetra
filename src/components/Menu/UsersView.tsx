import React, { useEffect, useRef } from 'react';
import {Platform, TextInput, View, Text, Image, TouchableOpacity} from 'react-native';
import { useRoute } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from '../../utilities/Common';
import { colors } from '../../styles';

const UsersView = ({title, style, onClick}) => {

	const [text, setText] = React.useState('');

	return (<TouchableOpacity style={[{
		width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
		height: Common.getLengthByIPhone7(60),
		borderRadius: Common.getLengthByIPhone7(10),
		overflow: 'hidden',
		paddingLeft: Common.getLengthByIPhone7(10),
		paddingRight: Common.getLengthByIPhone7(10),
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		backgroundColor: 'white',
		borderColor: '#eeeeee',
		borderWidth: 1,
	}, style]}
	onPress={() => {
		if (onClick) {
			onClick();
		}
	}}>
		<View style={{
			flexDirection: 'row',
			alignItems: 'center',
		}}>
			<View style={{
				height: Common.getLengthByIPhone7(40),
				width: Common.getLengthByIPhone7(40),
			}}>
				<Image
					source={require('./../../assets/ic-dictionary.png')}
					style={{
						height: Common.getLengthByIPhone7(40),
						width: Common.getLengthByIPhone7(40),
						resizeMode: 'contain',
					}}
				/>
			</View>
			<Text style={{
				color: colors.TEXT_COLOR,
				fontFamily: 'OpenSans-Bold',
				fontWeight: 'bold',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(14),
				marginLeft: Common.getLengthByIPhone7(12),
			}}
			numberOfLines={1}
			allowFontScaling={false}>
				Справочник сотрудников
			</Text>
		</View>
		<Image
			source={require('./../../assets/ic-arrow-right.png')}
			style={{
				height: Common.getLengthByIPhone7(12),
				width: Common.getLengthByIPhone7(6),
				marginRight: Common.getLengthByIPhone7(10),
				resizeMode: 'contain',
				tintColor: colors.GRAY_COLOR,
			}}
		/>
	</TouchableOpacity>);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
    
});

export default connect(mstp, mdtp)(UsersView);