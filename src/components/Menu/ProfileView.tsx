import React, {useEffect, useRef} from 'react';
import {
  Platform,
  TextInput,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import {config} from '../../constants';

const ProfileView = ({userProfile, style, onClick}) => {
  const [text, setText] = React.useState('');

  return (
    <TouchableOpacity
      style={[
        {
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
          height: Common.getLengthByIPhone7(60),
          borderRadius: Common.getLengthByIPhone7(10),
          overflow: 'hidden',
          paddingLeft: Common.getLengthByIPhone7(10),
          paddingRight: Common.getLengthByIPhone7(10),
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: 'rgba(255, 242, 211, 0.78)',
        },
        style,
      ]}
      onPress={() => {
        if (onClick) {
          onClick();
        }
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <View
          style={{
            height: Common.getLengthByIPhone7(40),
            width: Common.getLengthByIPhone7(40),
            borderRadius: Common.getLengthByIPhone7(20),
            borderColor: 'rgba(248, 170, 43, 0.3)',
            borderWidth: 2,
            overflow: 'hidden',
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {userProfile?.PERSONAL_PHOTO?.FILE?.SRC?.length ? (
            <Image
              source={{
                uri: config.BASE_URL + userProfile?.PERSONAL_PHOTO?.FILE?.SRC,
              }}
              style={{
                height: Common.getLengthByIPhone7(40),
                width: Common.getLengthByIPhone7(40),
                resizeMode: 'cover',
              }}
            />
          ) : (
            <Image
              source={require('./../../assets/ic-placeholder-user.png')}
              style={{
                width: Common.getLengthByIPhone7(20),
                height: Common.getLengthByIPhone7(20),
                resizeMode: 'contain',
              }}
            />
          )}
        </View>
        <Text
          style={{
            color: 'rgba(90, 67, 58, 0.8)',
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(14),
            marginLeft: Common.getLengthByIPhone7(12),
          }}
          numberOfLines={1}
          allowFontScaling={false}>
          {userProfile?.FULL_NAME}
        </Text>
      </View>
      <Image
        source={require('./../../assets/ic-arrow-right.png')}
        style={{
          height: Common.getLengthByIPhone7(12),
          width: Common.getLengthByIPhone7(6),
          marginRight: Common.getLengthByIPhone7(10),
          resizeMode: 'contain',
          tintColor: colors.YELLOW_ACTIVE_COLOR,
        }}
      />
    </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
  userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(ProfileView);
