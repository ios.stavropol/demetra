import React, { useEffect } from 'react';
import {TouchableOpacity, Image} from 'react-native';
import Common from '../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../styles';

const MenuButton = ({style}) => {

	const navigation = useNavigation();

	return (
		<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(32),
			height: Common.getLengthByIPhone7(32),
			alignItems: 'flex-end',
			justifyContent: 'center',
		}, style]}
		onPress={() => {
			navigation.navigate('MenuModal');
		}}>
			<Image
				source={require('./../assets/ic-dots.png')}
				style={{
					resizeMode: 'contain',
					width: Common.getLengthByIPhone7(24),
					height: Common.getLengthByIPhone7(24),
					tintColor: colors.BLUE_COLOR,
				}}
			/>
		</TouchableOpacity>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(MenuButton);