import React, {useEffect, useRef} from 'react';
import {
  ActivityIndicator,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';

let days = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ'];

const BirthdayView = ({
  style,
  navigation,
  getBirthdays,
  birthdays,
  onClick,
  setBirthdays,
}) => {
  const [body, setBody] = React.useState(
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(306),
          borderRadius: Common.getLengthByIPhone7(12),
          overflow: 'hidden',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
          padding: Common.getLengthByIPhone7(20),
        },
        style,
      ]}>
      <ActivityIndicator />
    </View>,
  );

  useEffect(() => {
    StorageHelper.getData('birthdays')
      .then(res => {
        if (res?.length) {
          res = JSON.parse(res);
          if (res) {
            setBirthdays(res);
            loadBirthDays();
          } else {
            loadBirthDays();
          }
        } else {
          loadBirthDays();
        }
      })
      .catch(err => {
        loadBirthDays();
      });
  }, []);

  const loadBirthDays = () => {
    getBirthdays()
      .then(() => {})
      .catch(err => {});
  };

  useEffect(() => {
    if (birthdays?.length) {
      let array = [];
      for (let i = 0; i < Math.min(3, birthdays.length); i++) {
        array.push(
          renderView(
            birthdays[i],
            {
              marginBottom: Common.getLengthByIPhone7(8),
            },
            () => {
              if (onClick) {
                onClick(birthdays[i]);
              }
            },
          ),
        );
      }
      setBody(
        <View
          style={[
            {
              width: Common.getLengthByIPhone7(0),
              // height: Common.getLengthByIPhone7(306),
              borderRadius: Common.getLengthByIPhone7(12),
              overflow: 'hidden',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: 'white',
              padding: Common.getLengthByIPhone7(20),
            },
            style,
          ]}>
          <Text
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
              marginBottom: Common.getLengthByIPhone7(10),
            }}
            numberOfLines={1}
            allowFontScaling={false}>
            {`Дни рождения`.toUpperCase()}
          </Text>
          {array}
          <TouchableOpacity
            style={{
              width: Common.getLengthByIPhone7(300),
              height: Common.getLengthByIPhone7(40),
              borderRadius: Common.getLengthByIPhone7(50),
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: colors.YELLOW_ACTIVE_COLOR,
              borderWidth: 1,
            }}
            onPress={() => {
              navigation.navigate('Birthdays');
            }}>
            <Text
              style={{
                color: colors.YELLOW_ACTIVE_COLOR,
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
              }}
              numberOfLines={1}
              allowFontScaling={false}>
              Смотреть все
            </Text>
          </TouchableOpacity>
        </View>,
      );
    }
  }, [birthdays]);

  const renderView = (data, style, action) => {
    console.warn('data: ', data);
    if (data) {
      let str = [];
      if (data?.LEGAL_INFO) {
        let keys = Object.keys(data?.LEGAL_INFO);
        for (let i = 0; i < Math.min(2, keys.length); i++) {
          str.push(
            <Text
              style={{
                marginTop: 5,
                color: colors.TEXT_COLOR,
                fontFamily: 'OpenSans-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(10),
              }}
              key={i}
              allowFontScaling={false}>
              {data?.LEGAL_INFO[keys[i]]?.user_work_position} (
              {data?.LEGAL_INFO[keys[i]]?.TITLE})
            </Text>,
          );
        }
      }

      let dd = data?.PERSONAL_BIRTHDAY;
      dd = dd.split('.');

      let currentYear = new Date().getFullYear();
      // let birthdayDate = new Date(currentYear, dd[1] - 1, dd[0]);
      let birthdayDate = new Date(
        currentYear,
        dd[1] - 1,
        dd[0],
        23,
        59,
        59,
        59,
      );
      var now = new Date().valueOf();
      if (birthdayDate.valueOf() < now) {
        birthdayDate.setFullYear(currentYear + 1);
      }

      dd = dd[0] + '.' + dd[1];
      return (
        <TouchableOpacity
          style={[
            {
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              height: Common.getLengthByIPhone7(74),
              borderRadius: Common.getLengthByIPhone7(15),
              padding: Common.getLengthByIPhone7(10),
              borderColor: colors.BORDER_COLOR,
              borderWidth: 1,
              flexDirection: 'row',
              alignItems: 'center',
            },
            style,
          ]}
          key={data?.ID}
          onPress={() => {
            if (action) {
              action();
            }
          }}>
          <View
            style={{
              width: Common.getLengthByIPhone7(44),
              height: Common.getLengthByIPhone7(54),
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#F9F5EB',
              borderRadius: Common.getLengthByIPhone7(6),
            }}>
            <Text
              style={{
                color: 'black',
                opacity: 0.25,
                fontFamily: 'OpenSans-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(10),
              }}
              allowFontScaling={false}>
              {days[birthdayDate.getDay()]}
            </Text>
            <View
              style={{
                marginTop: 2,
                width: Common.getLengthByIPhone7(38),
                height: Common.getLengthByIPhone7(30),
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                borderRadius: Common.getLengthByIPhone7(4),
              }}>
              <Text
                style={{
                  color: colors.YELLOW_ACTIVE_COLOR,
                  fontFamily: 'OpenSans-Bold',
                  fontWeight: 'bold',
                  textAlign: 'left',
                  fontSize: Common.getLengthByIPhone7(12),
                }}
                allowFontScaling={false}>
                {dd}
              </Text>
            </View>
          </View>
          <View
            style={{
              marginLeft: Common.getLengthByIPhone7(8),
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(112),
            }}>
            <Text
              style={{
                color: colors.TEXT_COLOR,
                fontFamily: 'OpenSans-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(12),
              }}
              allowFontScaling={false}>
              {data?.FULL_NAME}
            </Text>
            {str}
          </View>
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  };

  return body;
};

const mstp = (state: RootState) => ({
  birthdays: state.user.birthdays,
});

const mdtp = (dispatch: Dispatch) => ({
  getBirthdays: () => dispatch.user.getBirthdays(),
  setBirthdays: payload => dispatch.user.setBirthdays(payload),
});

export default connect(mstp, mdtp)(BirthdayView);
