import React, {useEffect, useRef} from 'react';
import {
  Platform,
  ActivityIndicator,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';

const ImportantView = ({
  navigation,
  style,
  setChangeTab,
  importants,
  readImportant,
  getImportant,
  refresh,
}) => {
  const [loading, setLoading] = React.useState(false);

  const [body, setBody] = React.useState(
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(326),
          borderRadius: Common.getLengthByIPhone7(12),
          overflow: 'hidden',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
          padding: Common.getLengthByIPhone7(20),
        },
        style,
      ]}>
      <ActivityIndicator />
    </View>,
  );
  const [unreaded, setUnreaded] = React.useState(0);

  useEffect(() => {
    setLoading(true);
    getImportant({
      pageNumber: 1,
      rowCount: 10,
    })
      .then(() => setLoading(false))
      .catch(err => setLoading(false));
  }, [refresh]);

  useEffect(() => {
    if (importants?.length) {
      let count = 0;
      for (let i = 0; i < importants?.length; i++) {
        if (!importants[i].IS_READ) {
          count++;
        }
      }
      setUnreaded(count);
    }
  }, [importants]);

  useEffect(() => {
    if (importants?.length && unreaded > 0) {
      let array = [];
      let count = 0;
      for (let i = 0; i < importants?.length; i++) {
        if (!importants[i].IS_READ) {
          array.push(
            renderView(
              importants[i],
              {
                marginLeft:
                  count === 0
                    ? Common.getLengthByIPhone7(20)
                    : Common.getLengthByIPhone7(8),
                marginRight:
                  i + 1 === importants.length
                    ? Common.getLengthByIPhone7(20)
                    : 0,
              },
              () => {
                navigation.navigate('ImportantPage2', {data: importants[i]});
              },
            ),
          );
        }
      }
      setBody(
        <View
          style={[
            {
              width: Common.getLengthByIPhone7(0),
              height: Common.getLengthByIPhone7(326),
              borderRadius: Common.getLengthByIPhone7(12),
              overflow: 'hidden',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: 'white',
              padding: Common.getLengthByIPhone7(20),
            },
            style,
          ]}>
          <View
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            }}>
            <Text
              style={{
                color: colors.TEXT_COLOR,
                fontFamily: 'OpenSans-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(12),
              }}
              numberOfLines={1}
              allowFontScaling={false}>
              {`Это важно`.toUpperCase()}
            </Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  color: colors.YELLOW_ACTIVE_COLOR,
                  fontFamily: 'OpenSans-SemiBold',
                  fontWeight: '600',
                  textAlign: 'left',
                  fontSize: Common.getLengthByIPhone7(12),
                }}
                numberOfLines={1}
                allowFontScaling={false}>
                Непрочитанных сообщений
              </Text>
              <View
                style={{
                  width: Common.getLengthByIPhone7(24),
                  height: Common.getLengthByIPhone7(24),
                  borderRadius: Common.getLengthByIPhone7(12),
                  backgroundColor: colors.YELLOW_ACTIVE_COLOR,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginLeft: Common.getLengthByIPhone7(7),
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontFamily: 'OpenSans-SemiBold',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(10),
                  }}
                  numberOfLines={1}
                  allowFontScaling={false}>
                  {unreaded}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              width: Common.getLengthByIPhone7(0),
              height: Common.getLengthByIPhone7(176),
              marginBottom: 0,
            }}>
            <ScrollView
              style={{
                width: Common.getLengthByIPhone7(0),
                height: Common.getLengthByIPhone7(176),
                marginBottom: 0,
              }}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              {array}
            </ScrollView>
          </View>
          <TouchableOpacity
            style={{
              width: Common.getLengthByIPhone7(300),
              height: Common.getLengthByIPhone7(40),
              borderRadius: Common.getLengthByIPhone7(50),
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: colors.YELLOW_ACTIVE_COLOR,
              borderWidth: 1,
            }}
            onPress={() => {
              setChangeTab(1);
              setTimeout(() => {
                navigation.navigate('News', {tab: 1});
              }, 400);
            }}>
            <Text
              style={{
                color: colors.YELLOW_ACTIVE_COLOR,
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
              }}
              numberOfLines={1}
              allowFontScaling={false}>
              Смотреть все
            </Text>
          </TouchableOpacity>
        </View>,
      );
    } else {
      if (loading) {
        setBody(
          <View
            style={[
              {
                width: Common.getLengthByIPhone7(0),
                height: Common.getLengthByIPhone7(326),
                borderRadius: Common.getLengthByIPhone7(12),
                overflow: 'hidden',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
                padding: Common.getLengthByIPhone7(20),
              },
              style,
            ]}>
            <ActivityIndicator />
          </View>,
        );
      } else {
        setBody(null);
      }
    }
  }, [unreaded, loading]);

  const renderView = (data, style, action) => {
    let date = new Date(data?.LOG_DATE);
    let dd = date.getDate();
    dd = dd < 10 ? '0' + dd : dd;

    let mm = date.getMonth() + 1;
    mm = mm < 10 ? '0' + mm : mm;

    let yyyy = date.getFullYear();

    let hh = date.getHours();
    hh = hh < 10 ? '0' + hh : hh;

    let ii = date.getMinutes();
    ii = ii < 10 ? '0' + ii : ii;

    let str = dd + '-' + mm + '-' + yyyy + ' ' + hh + ':' + ii;
    return (
      <TouchableOpacity
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: Common.getLengthByIPhone7(176),
            borderRadius: Common.getLengthByIPhone7(12),
            overflow: 'hidden',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            padding: Common.getLengthByIPhone7(20),
            paddingBottom: Common.getLengthByIPhone7(28),
            backgroundColor: '#f7f7f7',
          },
          style,
        ]}
        activeOpacity={0.7}
        onPress={() => {
          if (action) {
            action();
          }
        }}>
        {!data?.IS_READ ? (
          <LinearGradient
            colors={['#f7f7f7', '#F9F5EB']}
            start={{x: 0.0, y: 0.5}}
            end={{x: 1.0, y: 0.5}}
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              height: Common.getLengthByIPhone7(176),
              position: 'absolute',
              left: 0,
              top: 0,
            }}
          />
        ) : (
          <View
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              height: Common.getLengthByIPhone7(176),
              position: 'absolute',
              left: 0,
              top: 0,
              backgroundColor: '#f8f8f8',
            }}
          />
        )}
        <View
          style={{
            width:
              Common.getLengthByIPhone7(320) - Common.getLengthByIPhone7(40),
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <Text
            style={{
              color: colors.GRAY_COLOR,
              fontFamily: 'OpenSans-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(10),
            }}
            numberOfLines={1}
            allowFontScaling={false}>
            {str}
          </Text>
          <Image
            source={
              data?.IS_READ
                ? require('./../../assets/ic-tick.png')
                : require('./../../assets/ic-info.png')
            }
            style={{
              width: Common.getLengthByIPhone7(24),
              height: Common.getLengthByIPhone7(24),
              resizeMode: 'contain',
            }}
          />
        </View>
        <Text
          style={{
            color: colors.TEXT_COLOR,
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(16),
          }}
          numberOfLines={2}
          allowFontScaling={false}>
          {data?.TITLE}
        </Text>
        {data?.IS_READ ? null : (
          <TouchableOpacity
            style={{
              width: Common.getLengthByIPhone7(139),
              height: Common.getLengthByIPhone7(40),
              borderRadius: Common.getLengthByIPhone7(50),
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: colors.YELLOW_ACTIVE_COLOR,
            }}
            onPress={() => {
              readImportant(data?.SOURCE_ID)
                .then(() => {
                  getImportant({
                    pageNumber: 1,
                    rowCount: 10,
                  });
                })
                .catch(err => {});
            }}>
            <Text
              style={{
                color: 'white',
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
              }}
              numberOfLines={1}
              allowFontScaling={false}>
              Прочитано
            </Text>
          </TouchableOpacity>
        )}
      </TouchableOpacity>
    );
  };

  return body;
};

const mstp = (state: RootState) => ({
  importants: state.user.importants,
});

const mdtp = (dispatch: Dispatch) => ({
  readImportant: payload => dispatch.user.readImportant(payload),
  getImportant: payload => dispatch.user.getImportant(payload),
  setChangeTab: payload => dispatch.buttons.setChangeTab(payload),
});

export default connect(mstp, mdtp)(ImportantView);
