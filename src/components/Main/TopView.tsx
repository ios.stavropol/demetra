import React, {useEffect, useRef} from 'react';
import {
  Platform,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import {BlurView} from '@react-native-community/blur';

const TopView = ({style, onClick}) => {
  const [text, setText] = React.useState('');

  const renderView = (title, value, style, action) => {
    return (
      <TouchableOpacity
        style={[
          {
            marginTop: Common.getLengthByIPhone7(10),
            width: Common.getLengthByIPhone7(148),
            height: Common.getLengthByIPhone7(90),
            borderRadius: Common.getLengthByIPhone7(10),
            overflow: 'hidden',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            padding: Common.getLengthByIPhone7(10),
            paddingBottom: Common.getLengthByIPhone7(20),
          },
          style,
        ]}
        activeOpacity={0.7}
        onPress={() => {
          if (action) {
            action();
          }
        }}>
        {Platform.OS === 'ios' ? (
          <BlurView
            style={{
              width: Common.getLengthByIPhone7(148),
              height: Common.getLengthByIPhone7(90),
              borderRadius: Common.getLengthByIPhone7(10),
              position: 'absolute',
              left: 0,
              top: 0,
            }}
            blurType="light"
            blurAmount={10}
            reducedTransparencyFallbackColor="white"
          />
        ) : (
          <View
            style={{
              width: Common.getLengthByIPhone7(148),
              height: Common.getLengthByIPhone7(90),
              borderRadius: Common.getLengthByIPhone7(10),
              position: 'absolute',
              backgroundColor: 'white',
              left: 0,
              top: 0,
              opacity: 0.5,
            }}
          />
        )}
        <View
          style={{
            width: Common.getLengthByIPhone7(24),
            height: Common.getLengthByIPhone7(24),
            borderRadius: Common.getLengthByIPhone7(12),
            backgroundColor: colors.YELLOW_ACTIVE_COLOR,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              color: 'white',
              fontFamily: 'OpenSans-SemiBold',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(10),
            }}
            numberOfLines={1}
            allowFontScaling={false}>
            {value}
          </Text>
        </View>
        <Text
          style={{
            color: 'rgba(90, 67, 58, 0.8)',
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(14),
          }}
          numberOfLines={1}
          allowFontScaling={false}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(320),
          borderBottomLeftRadius: Common.getLengthByIPhone7(12),
          borderBottomRightRadius: Common.getLengthByIPhone7(12),
          overflow: 'hidden',
          justifyContent: 'flex-end',
        },
        style,
      ]}>
      <Image
        source={require('./../../assets/ic-main-img.png')}
        style={{
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(320),
          resizeMode: 'cover',
          position: 'absolute',
          left: 0,
          top: 0,
        }}
      />
      {/* <View style={{
			width: Common.getLengthByIPhone7(0),
			height: Common.getLengthByIPhone7(120),
			marginBottom: 0,
		}}>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				height: Common.getLengthByIPhone7(120),
				marginBottom: 0,
			}}
			horizontal={true}
			showsHorizontalScrollIndicator={false}
			contentContainerStyle={{
				alignItems: 'flex-start',
				justifyContent: 'center',
			}}>
				{renderView('Согласования', 2, {
					marginLeft: Common.getLengthByIPhone7(20),
				}, () => {

				})}
				{renderView('Заявки', 4, {
					marginLeft: Common.getLengthByIPhone7(8),
				}, () => {

				})}
				{renderView('Задачи', 1, {
					marginLeft: Common.getLengthByIPhone7(8),
					marginRight: Common.getLengthByIPhone7(20),
				}, () => {

				})}
			</ScrollView>
		</View> */}
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(TopView);
