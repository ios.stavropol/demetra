import React, {useEffect, useRef} from 'react';
import {
  ActivityIndicator,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from '../../utilities/Common';
import {colors} from '../../styles';
import LinearGradient from 'react-native-linear-gradient';
import {config} from '../../constants';

const NewsView = ({style, news, setChangeTab, navigation}) => {
  const [body, setBody] = React.useState(
    <View
      style={[
        {
          width: Common.getLengthByIPhone7(0),
          height: Common.getLengthByIPhone7(306),
          borderRadius: Common.getLengthByIPhone7(12),
          overflow: 'hidden',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
          padding: Common.getLengthByIPhone7(20),
        },
        style,
      ]}>
      <ActivityIndicator />
    </View>,
  );

  useEffect(() => {
    if (news?.length) {
      let array = [];
      for (let i = 0; i < Math.min(3, news.length); i++) {
        array.push(
          renderView(
            news[i],
            {
              marginLeft:
                i === 0
                  ? Common.getLengthByIPhone7(20)
                  : Common.getLengthByIPhone7(8),
              marginRight:
                i + 1 === Math.min(3, news.length)
                  ? Common.getLengthByIPhone7(20)
                  : 0,
            },
            () => {},
          ),
        );
      }
      setBody(
        <View
          style={[
            {
              width: Common.getLengthByIPhone7(0),
              height: Common.getLengthByIPhone7(306),
              borderRadius: Common.getLengthByIPhone7(12),
              overflow: 'hidden',
              justifyContent: 'space-between',
              alignItems: 'center',
              backgroundColor: 'white',
              padding: Common.getLengthByIPhone7(20),
            },
            style,
          ]}>
          <Text
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
            }}
            numberOfLines={1}
            allowFontScaling={false}>
            {`Новости`.toUpperCase()}
          </Text>
          <View
            style={{
              width: Common.getLengthByIPhone7(0),
              height: Common.getLengthByIPhone7(180),
              marginBottom: 0,
            }}>
            <ScrollView
              style={{
                width: Common.getLengthByIPhone7(0),
                height: Common.getLengthByIPhone7(180),
                marginBottom: 0,
              }}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={{
                alignItems: 'flex-start',
                justifyContent: 'center',
              }}>
              {array}
            </ScrollView>
          </View>
          <TouchableOpacity
            style={{
              width: Common.getLengthByIPhone7(300),
              height: Common.getLengthByIPhone7(40),
              borderRadius: Common.getLengthByIPhone7(50),
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: colors.YELLOW_ACTIVE_COLOR,
              borderWidth: 1,
            }}
            onPress={() => {
              setChangeTab(0);
              setTimeout(() => {
                navigation.navigate('News', {tab: 0});
              }, 400);
            }}>
            <Text
              style={{
                color: colors.YELLOW_ACTIVE_COLOR,
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
              }}
              numberOfLines={1}
              allowFontScaling={false}>
              Смотреть все
            </Text>
          </TouchableOpacity>
        </View>,
      );
    }
  }, [news]);

  const renderView = (data, style, action) => {
    return (
      <TouchableOpacity
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: Common.getLengthByIPhone7(180),
            borderRadius: Common.getLengthByIPhone7(12),
            overflow: 'hidden',
            justifyContent: 'flex-end',
            alignItems: 'flex-start',
            padding: Common.getLengthByIPhone7(20),
            paddingBottom: Common.getLengthByIPhone7(12),
            backgroundColor: '#f7f7f7',
          },
          style,
        ]}
        activeOpacity={0.7}
        onPress={() => {
          navigation.navigate('NewsPage2', {data: data});
        }}>
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
            height: Common.getLengthByIPhone7(180),
            position: 'absolute',
            left: 0,
            top: 0,
          }}>
          {data?.FIRST_IMG?.length ? (
            <Image
              source={{uri: config.BASE_URL + data?.FIRST_IMG}}
              style={{
                width:
                  Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                height: Common.getLengthByIPhone7(180),
                resizeMode: 'cover',
                position: 'absolute',
                left: 0,
                top: 0,
              }}
            />
          ) : (
            <Image
              source={require('./../../assets/ic-placeholder-news.png')}
              style={{
                width:
                  Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
                height: Common.getLengthByIPhone7(180),
                resizeMode: 'cover',
                position: 'absolute',
                left: 0,
                top: 0,
              }}
            />
          )}
          <LinearGradient
            colors={['rgba(117, 77, 58, 0)', 'rgba(35, 22, 16, 1)']}
            // start={{x: 0.0, y: 0.5}} end={{x: 1.0, y: 0.5}}
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              height: Common.getLengthByIPhone7(180),
              position: 'absolute',
              left: 0,
              top: 0,
            }}
          />
        </View>
        <Text
          style={{
            color: 'white',
            fontFamily: 'OpenSans-Bold',
            fontWeight: 'bold',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(12),
          }}
          allowFontScaling={false}>
          {data?.TITLE}
        </Text>
        <Text
          style={{
            marginTop: Common.getLengthByIPhone7(8),
            color: colors.GRAY_COLOR,
            fontFamily: 'OpenSans-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          numberOfLines={1}
          allowFontScaling={false}>
          {Common.getRusDateTime(new Date(data?.LOG_DATE))}
        </Text>
      </TouchableOpacity>
    );
  };

  return body;
};

const mstp = (state: RootState) => ({
  news: state.user.news,
});

const mdtp = (dispatch: Dispatch) => ({
  setChangeTab: payload => dispatch.buttons.setChangeTab(payload),
});

export default connect(mstp, mdtp)(NewsView);
