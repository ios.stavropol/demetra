// 0 - dev разработчика
// 1 - dev заказчика
// 2 - prod заказчика
// export const SERVER = 2;
// export let BASE_URL = SERVER === 0 ? 'https://demetra.dev-vps.ru' : (SERVER === 1 ? 'https://devportal.dholding.ru' : 'https://portal.dholding.ru');
// export const ONESIGNAL_KEY = SERVER === 0 ? 'cbae6757-a067-45bd-89fb-3a7c6a8a649f' : (SERVER === 1 ? '97893df2-73b3-40bf-a3fc-812879c22d2e' : 'd210189a-605a-4a11-adf8-15a15ce28ed2');
// export const VERSION = '1.2022.09.28.01';
// export const APP_NAME = 'DHome';

const SERVER = 2;
// let BASE_URL = 'https://demetra.dev-vps.ru';
// let BASE_URL = 'https://devportal.dholding.ru';
let BASE_URL = 'https://portal.dholding.ru';
let ONESIGNAL_KEY = SERVER === 0 ? 'cbae6757-a067-45bd-89fb-3a7c6a8a649f' : (SERVER === 1 ? '97893df2-73b3-40bf-a3fc-812879c22d2e' : 'd210189a-605a-4a11-adf8-15a15ce28ed2');
const VERSION = '1.2022.10.18.03';
const APP_NAME = 'DHome';

export const config = {
	BASE_URL,
	ONESIGNAL_KEY,
	VERSION,
	APP_NAME,
};