import axios, { AxiosError, AxiosRequestConfig } from 'axios';
import { config } from './../../constants';
import { commonRequests } from './Requests';

export interface ApiResponse<T> {
	status: number;
	data: T;
	points: T;
	message: string;
}

axios.interceptors.request.use(
	async (config) => {
		console.warn('Request', config.headers);
		const newConfig: AxiosRequestConfig = {
			...config,
			baseURL: config.BASE_URL,
			headers: { ...config.headers, 'Content-Type': 'application/json', 'Accept': 'application/json' },
		};

		return newConfig;
	},
	(error: AxiosError) => {
		return Promise.reject(error);
	},
);

class APIService {
	common = commonRequests;

	setToken = (token: string) => {
		console.warn('APIService -> setToken -> token', token);
		axios.defaults.withCredentials = true;
	};

	loadToken = (token: string) => {
		console.warn('APIService -> loadToken -> token', token);
		let str = config.BASE_URL;
		str = str.split('https://');
		console.warn('str: ', str);
		console.warn('PHPSESSID=' + token + '; path=/; domain=' + str[1] + '; HttpOnly');
		// axios.defaults.headers['Cookie'] = 'PHPSESSID=' + token + '; path=/; domain=' + str[1] + '; HttpOnly';
		axios.defaults.headers['Cookie'] = 'PHPSESSID=' + token;
	};

	loadDeviceId = (deviceId: string) => {
		console.warn('APIService -> loadDeviceId -> deviceId', deviceId);
		axios.defaults.headers['deviceId'] = deviceId;
	};

	clearToken = () => {
		console.warn('clearToken');
		axios.defaults.headers['Cookie'] = {};
	};
}

const API = new APIService();
export default API;
