import axios from 'axios';
import { config } from './../../../constants';

class CommonAPI {
	login = (payload) => {
		let config2 = {
			method: 'get',
			url: config.BASE_URL + '/api/user/login',
			headers: { 'Authorization': 'Basic '+ payload}
		};
		return axios(config2);
	};

	sendCode = (otpCode) => {
		let params = JSON.stringify({
			otpCode,
		});
		return axios.post(config.BASE_URL + '/api/user/auth', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	getProfile = () => {
		console.warn('config.BASE_URL: ', config.BASE_URL);
		return axios.get(config.BASE_URL + '/api/user/getMe');
	};

	restorePassword = (email) => {
		let params = JSON.stringify({
			email,
		});
		return axios.post(config.BASE_URL + '/api/user/password/restore', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};	

	getImportant = (pageNumber: number, rowCount: number) => {
		let params = JSON.stringify({
			pageNumber,
			rowCount,
		});
		return axios.post(config.BASE_URL + '/api/livefeed/important/get', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	getImportantById = (id: number) => {
		let params = JSON.stringify({
			id,
		});
		return axios.post(config.BASE_URL + '/api/livefeed/important/getById', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	readImportant = (sourceId) => {
		let params = JSON.stringify({
			sourceId: parseInt(sourceId),
		});
		return axios.post(config.BASE_URL + '/api/livefeed/important/read', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	getNews = (pageNumber: number, rowCount: number) => {
		let params = JSON.stringify({
			pageNumber,
			rowCount,
		});
		return axios.post(config.BASE_URL + '/api/livefeed/news/get', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	getNewsById = (id: number) => {
		let params = JSON.stringify({
			id,
		});
		return axios.post(config.BASE_URL + '/api/livefeed/news/getById', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	getUserById = (userId: number) => {
		let params = JSON.stringify({
			userId,
		});
		return axios.post(config.BASE_URL + '/api/user/getById', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	getBirthdays = () => {
		return axios.get(config.BASE_URL + '/api/user/birthdays');
	};

	getUsers = () => {
		return axios.get(config.BASE_URL + '/api/user/getList');
	};

	getKnowledgebase = () => {
		return axios.get(config.BASE_URL + '/api/knowledgebase/getList');
	};

	search = (query: string) => {
		let params = JSON.stringify({
			q: query,
		});
		return axios.post(config.BASE_URL + '/api/search/query', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	changePassword = (newPassword: string) => {
		let params = JSON.stringify({
			newPassword,
		});
		return axios.post(config.BASE_URL + '/api/user/password/change', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	getStructure = () => {
		return axios.get(config.BASE_URL + '/api/structure/getTree');
	};

	getDepartmentEmployee = (departmentId: string) => {
		let params = JSON.stringify({
			departmentId,
		});
		return axios.post(config.BASE_URL + '/api/structure/getDepartmentEmployee', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	setImportantNotification = (enable: boolean) => {
		let params = JSON.stringify({
			enable,
		});
		return axios.post(config.BASE_URL + '/api/user/setImportantNotification', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};

	setNewsNotification = (enable: boolean) => {
		let params = JSON.stringify({
			enable,
		});
		return axios.post(config.BASE_URL + '/api/user/setNewsNotification', params, {
			headers: {
				'Content-Type': 'application/json',
			}
		});
	};
}

const commonRequests = new CommonAPI();
export default commonRequests;
