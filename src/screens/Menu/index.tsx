import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  StatusBar,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import ProfileView from '../../components/Menu/ProfileView';
import UsersView from '../../components/Menu/UsersView';

let count = 0;

const MenuScreen = ({}) => {
  const navigation = useNavigation();

  const [email, setEmail] = React.useState('');

  useEffect(() => {}, []);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        // setTimeout(() => {
        //   count = 0;
        // }, 2000);

        // if (count === 0) {
        //   count++;
        // } else if (count === 1) {
        //   BackHandler.exitApp();
        // }
        navigation.navigate('Main');
        return true;
      });
    }, []),
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <ProfileView
        style={{}}
        onClick={() => {
          navigation.navigate('Profile');
        }}
      />
      <UsersView
        style={{
          marginTop: Common.getLengthByIPhone7(10),
        }}
        onClick={() => {
          navigation.navigate('Users');
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({
  users: state.user.users,
});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(MenuScreen);
