import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  Linking,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';
import {config} from '../../constants';
import LinearGradient from 'react-native-linear-gradient';
import RenderHtml from 'react-native-render-html';

const ImportantPageScreen = ({route, readImportant, getImportantById}) => {
  const navigation = useNavigation();

  const [news, setNews] = React.useState(null);
  const [body, setBody] = React.useState(null);

  useEffect(() => {
    getImportantById(route?.params?.data.ID)
      .then(res => {
        setNews(res);
      })
      .catch(err => {});
  }, []);

  const openURL = url => {
    if (url.indexOf(config.BASE_URL) !== -1) {
      url = url.split(config.BASE_URL);
      url = url[1];
      if (route.name === 'ImportantPage2') {
        navigation.navigate('Browser2', {data: url});
      } else if (route.name === 'ImportantPage3') {
        navigation.navigate('Browser3', {data: url});
      } else if (route.name === 'ImportantPage') {
        navigation.navigate('Browser4', {data: url});
      }
      return;
    }
    Linking.openURL(url);
  };

  useEffect(() => {
    if (news) {
      setBody(
        <ScrollView
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
          }}
          contentContainerStyle={{
            alignItems: 'center',
          }}>
          <View
            style={{
              marginTop: Common.getLengthByIPhone7(20),
              marginBottom: Common.getLengthByIPhone7(20),
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              borderRadius: Common.getLengthByIPhone7(12),
              overflow: 'hidden',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              padding: Common.getLengthByIPhone7(20),
              paddingBottom: Common.getLengthByIPhone7(28),
              backgroundColor: '#f7f7f7',
            }}>
            {news?.IS_READ ? (
              <View
                style={{
                  width:
                    Common.getLengthByIPhone7(0) -
                    Common.getLengthByIPhone7(40),
                  position: 'absolute',
                  left: 0,
                  top: 0,
                  bottom: 0,
                  backgroundColor: '#f8f8f8',
                }}
              />
            ) : (
              <LinearGradient
                colors={['#f7f7f7', '#F9F5EB']}
                start={{x: 0.0, y: 0.5}}
                end={{x: 1.0, y: 0.5}}
                style={{
                  width:
                    Common.getLengthByIPhone7(0) -
                    Common.getLengthByIPhone7(40),
                  position: 'absolute',
                  left: 0,
                  top: 0,
                  bottom: 0,
                }}
              />
            )}
            <View
              style={{
                width:
                  Common.getLengthByIPhone7(320) -
                  Common.getLengthByIPhone7(40),
                alignItems: 'center',
                justifyContent: 'space-between',
                flexDirection: 'row',
              }}>
              <Text
                style={{
                  color: colors.GRAY_COLOR,
                  fontFamily: 'OpenSans-Regular',
                  fontWeight: 'normal',
                  textAlign: 'left',
                  fontSize: Common.getLengthByIPhone7(10),
                }}
                numberOfLines={1}
                allowFontScaling={false}>
                {news?.LOG_DATE}
              </Text>
              <Image
                source={
                  news?.IS_READ
                    ? require('./../../assets/ic-tick.png')
                    : require('./../../assets/ic-info.png')
                }
                style={{
                  width: Common.getLengthByIPhone7(24),
                  height: Common.getLengthByIPhone7(24),
                  resizeMode: 'contain',
                }}
              />
            </View>
            <Text
              style={{
                marginTop: Common.getLengthByIPhone7(10),
                color: colors.TEXT_COLOR,
                fontFamily: 'OpenSans-Bold',
                fontWeight: 'bold',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(16),
              }}
              // numberOfLines={2}
              allowFontScaling={false}>
              {news?.TITLE}
            </Text>
            <RenderHtml
              baseStyle={{
                width:
                  Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
                marginTop: Common.getLengthByIPhone7(15),
              }}
              contentWidth={
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80)
              }
              source={{
                baseUrl: config.BASE_URL,
                html: news?.MESSAGE,
              }}
              tagsStyles={{
                body: {
                  whiteSpace: 'normal',
                  color: 'black',
                },
              }}
              renderersProps={{
                a: {
                  onPress(event, url, htmlAttribs, target) {
                    console.warn('url: ', url);
                    console.warn('route: ', route);
                    if (url.indexOf(config.BASE_URL) !== -1) {
                      if (url.indexOf('personal/user/') !== -1) {
                        let ids = url.split('personal/user/');
                        if (ids?.length === 2) {
                          ids = ids[1].split('/');
                          console.warn(ids);
                          if (ids?.length === 2 && ids[1]?.length === 0) {
                            if (route.name === 'ImportantPage2') {
                              navigation.navigate('AlienProfile3', {
                                data: {
                                  ID: ids[0],
                                },
                              });
                            } else if (route.name === 'ImportantPage3') {
                              navigation.navigate('AlienProfile2', {
                                data: {
                                  ID: ids[0],
                                },
                              });
                            } else {
                              navigation.navigate('AlienProfile4', {
                                data: {
                                  ID: ids[0],
                                },
                              });
                            }
                          } else {
                            openURL(url);
                          }
                        }
                      } else if (url.indexOf('/knowledge/') !== -1) {
                        let ids = url.split('/knowledge/');
                        if (ids?.length === 2) {
                          ids = '/mobile/knowledge/' + ids[1];
                          if (route.name === 'ImportantPage2') {
                            navigation.navigate('Browser2', {data: ids});
                          } else if (route.name === 'ImportantPage3') {
                            navigation.navigate('Browser3', {data: ids});
                          } else {
                            navigation.navigate('Browser4', {data: ids});
                          }
                        }
                      } else {
                        openURL(url);
                      }
                    } else {
                      openURL(url);
                    }
                  },
                },
              }}
            />
            {news?.IS_READ ? (
              <Text
                style={{
                  color: '#c4c4c4',
                  fontFamily: 'OpenSans-Regular',
                  fontWeight: 'normal',
                  textAlign: 'left',
                  fontSize: Common.getLengthByIPhone7(12),
                }}
                numberOfLines={1}
                allowFontScaling={false}>
                Прочитано 23.12.2021 в 13:31
              </Text>
            ) : (
              <TouchableOpacity
                style={{
                  width: Common.getLengthByIPhone7(139),
                  height: Common.getLengthByIPhone7(40),
                  borderRadius: Common.getLengthByIPhone7(50),
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: colors.YELLOW_ACTIVE_COLOR,
                }}
                onPress={() => {
                  readImportant(news?.SOURCE_ID)
                    .then(() => {
                      let dd = JSON.parse(JSON.stringify(news));
                      dd.IS_READ = !dd.IS_READ;
                      setNews(dd);
                    })
                    .catch(err => {});
                }}>
                <Text
                  style={{
                    color: 'white',
                    fontFamily: 'OpenSans-SemiBold',
                    fontWeight: 'normal',
                    textAlign: 'left',
                    fontSize: Common.getLengthByIPhone7(14),
                  }}
                  numberOfLines={1}
                  allowFontScaling={false}>
                  Прочитать
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>,
      );
    }
  }, [news]);

  BackHandler.addEventListener('hardwareBackPress', function () {
    navigation.goBack();
    return true;
  });

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      {body}
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  readImportant: payload => dispatch.user.readImportant(payload),
  getImportantById: payload => dispatch.user.getImportantById(payload),
});

export default connect(mstp, mdtp)(ImportantPageScreen);
