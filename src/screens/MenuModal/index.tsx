import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  Text,
  Platform,
  View,
  TouchableOpacity,
  Image,
  Animated,
  Easing,
  Alert,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors} from '../../styles';
import {useNavigation} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/native';
import {isIphoneX} from 'react-native-iphone-x-helper';
import {config} from '../../constants';

const MenuModalScreen = ({route, accessToken}) => {
  const navigation = useNavigation();

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  const renderRow = (title, icon, style, action) => {
    return (
      <TouchableOpacity
        style={[
          {
            height: Common.getLengthByIPhone7(24),
            flexDirection: 'row',
            alignItems: 'center',
          },
          style,
        ]}
        onPress={() => {
          if (action) {
            action();
          }
        }}>
        <Image
          source={icon}
          style={{
            width: Common.getLengthByIPhone7(24),
            height: Common.getLengthByIPhone7(24),
            resizeMode: 'contain',
            tintColor: colors.GRAY_COLOR,
          }}
        />
        <Text
          style={{
            marginLeft: Common.getLengthByIPhone7(7),
            fontSize: Common.getLengthByIPhone7(14),
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '700',
            color: colors.TEXT_COLOR,
          }}
          allowFontScaling={false}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <TouchableOpacity
      style={{
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
      }}
      activeOpacity={1}
      onPress={() => {
        navigation.goBack();
      }}>
      <View
        style={{
          marginTop: Common.getLengthByIPhone7(54) + (isIphoneX() ? 35 : 0),
          marginRight: Common.getLengthByIPhone7(10),
          borderRadius: Common.getLengthByIPhone7(10),
          // width: Common.getLengthByIPhone7(163),
          // height: Common.getLengthByIPhone7(96),
          padding: Common.getLengthByIPhone7(16),
          // paddingBottom: Common.getLengthByIPhone7(40),
          alignItems: 'flex-start',
          justifyContent: 'center',
          backgroundColor: '#f2f2f2',
        }}>
        {renderRow(
          'Безопасность',
          require('./../../assets/ic-lock.png'),
          {
            marginBottom: Common.getLengthByIPhone7(16),
          },
          () => {
            navigation.goBack();
            setTimeout(() => {
              navigation.navigate('Security');
            }, 200);
          },
        )}
        {renderRow(
          'Настройки',
          require('./../../assets/ic-settings.png'),
          {
            marginBottom: Common.getLengthByIPhone7(16),
          },
          () => {
            navigation.goBack();
            setTimeout(() => {
              navigation.navigate('Settings');
            }, 200);
          },
        )}
        {renderRow(
          'О приложении',
          require('./../../assets/ic-info2.png'),
          {
            marginBottom: Common.getLengthByIPhone7(16),
          },
          () => {
            navigation.goBack();
            setTimeout(() => {
              Alert.alert(
                config.APP_NAME,
                'Версия: ' +
                  config.VERSION +
                  `\n` +
                  'Сервер: ' +
                  config.BASE_URL,
              );
            }, 200);
          },
        )}
        <View
          style={{
            width: Common.getLengthByIPhone7(140),
            height: 1,
            backgroundColor: 'rgba(202, 206, 211, 0.3)',
          }}
        />
        {renderRow(
          'Выход',
          require('./../../assets/ic-exit.png'),
          {
            marginTop: Common.getLengthByIPhone7(16),
          },
          () => {
            StorageHelper.saveData('token', '');
            navigation.goBack();
            API.loadToken(null);
            setTimeout(() => {
              navigation.navigate('Login');
            }, 100);
          },
        )}
      </View>
    </TouchableOpacity>
  );
};

const mstp = (state: RootState) => ({
  accessToken: state.user.accessToken,
});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(MenuModalScreen);
