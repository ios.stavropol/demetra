import React, {useCallback, useEffect, useRef, useState} from 'react';
import {
  Platform,
  View,
  BackHandler,
  TouchableOpacity,
  RefreshControl,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import ProfileView from '../../components/Main/ProfileView';
import TopView from '../../components/Main/TopView';
import ImportantView from '../../components/Main/ImportantView';
import NewsView from '../../components/Main/NewsView';
import BirthdayView from '../../components/Main/BirthdayView';
import OneSignal from 'react-native-onesignal';
import {config} from '../../constants';
import {isIphoneX} from 'react-native-iphone-x-helper';
import {
  checkNotifications,
  requestNotifications,
  PERMISSIONS,
  RESULTS,
} from 'react-native-permissions';

let count = 0;

const MainScreen = ({getNews, setShowAlert, getImportant}) => {
  const navigation = useNavigation();

  const [loading, setLoading] = React.useState(false);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        setTimeout(() => {
          count = 0;
        }, 2000);

        if (count === 0) {
          count++;
        } else if (count === 1) {
          BackHandler.exitApp();
        }
        return true;
      });
    }, []),
  );

  useEffect(() => {
    checkNotifications()
      .then(({status, settings}) => {
        if (status === RESULTS.GRANTED) {
          notify();
        } else {
          requestNotifications(['alert', 'sound', 'badge'])
            .then(({status, settings}) => {
              notify();
            })
            .catch(err => {
              console.warn('requestNotifications err: ', status);
              notify();
            });
        }
      })
      .catch(err => {
        console.warn('checkNotifications err: ', err);
      });
  }, []);

  const notify = () => {
    /* O N E S I G N A L   S E T U P */
    OneSignal.setAppId(config.ONESIGNAL_KEY);
    OneSignal.setLogLevel(6, 0);
    OneSignal.setRequiresUserPrivacyConsent(false);
    OneSignal.promptForPushNotificationsWithUserResponse(response => {});

    /* O N E S I G N A L  H A N D L E R S */
    OneSignal.setNotificationWillShowInForegroundHandler(
      notificationReceivedEvent => {
        console.warn(
          'setNotificationWillShowInForegroundHandler: ' +
            JSON.stringify(notificationReceivedEvent),
        );
        let data = notificationReceivedEvent.notification.additionalData.type;
      },
    );
    OneSignal.setNotificationOpenedHandler(notification => {
      console.warn(
        'setNotificationOpenedHandler: ' + JSON.stringify(notification),
      );
      getImportant({
        pageNumber: 1,
        rowCount: 10,
      });
      getNews({
        pageNumber: 1,
        rowCount: 10,
      });
      if (notification.notification.additionalData.type === 'news') {
        navigation.navigate('NewsPage2', {
          data: {ID: notification.notification.additionalData.ID},
        });
      } else {
        navigation.navigate('ImportantPage2', {
          data: {ID: notification.notification.additionalData.ID},
        });
      }
    });
    OneSignal.setInAppMessageClickHandler(event => {
      console.warn('setInAppMessageClickHandler: ' + JSON.stringify(event));
      // this.OSLog("OneSignal IAM clicked:", event);
    });
    OneSignal.addEmailSubscriptionObserver(event => {
      console.warn('addEmailSubscriptionObserver: ' + JSON.stringify(event));
      // this.OSLog("OneSignal: email subscription changed: ", event);
    });
    OneSignal.addSubscriptionObserver(event => {
      console.warn('addSubscriptionObserver: ' + JSON.stringify(event));
      // this.OSLog("OneSignal: subscription changed:", event);
      // this.setState({ isSubscribed: event.to.isSubscribed})
    });
    OneSignal.addPermissionObserver(event => {
      console.warn('addPermissionObserver: ' + JSON.stringify(event));
      // this.OSLog("OneSignal: permission changed:", event);
    });

    OneSignal.getDeviceState()
      .then(device => {})
      .catch(err => {});
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BORDER_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <ScrollView
        style={{
          flex: 1,
          width: Common.getLengthByIPhone7(0),
        }}
        // refreshControl={
        //   <RefreshControl
        //     refreshing={loading}
        //     onRefresh={() => {
        //       console.warn('loading: ', loading);
        //       setLoading(!loading);
        //     }}
        //   />
        // }
        contentContainerStyle={{
          alignItems: 'center',
        }}>
        <TopView style={{}} />
        <ImportantView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          navigation={navigation}
        />
        <NewsView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          navigation={navigation}
        />
        <BirthdayView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
            marginBottom: Common.getLengthByIPhone7(10),
          }}
          navigation={navigation}
          onClick={obj => {
            navigation.navigate('AlienProfile3', {
              data: {ID: obj.ID},
              from: 'main',
            });
          }}
        />
      </ScrollView>
      <ProfileView
        style={{
          top: isIphoneX()
            ? Common.getLengthByIPhone7(45)
            : Common.getLengthByIPhone7(35),
          position: 'absolute',
        }}
        navigation={navigation}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  getImportant: payload => dispatch.user.getImportant(payload),
  getNews: payload => dispatch.user.getNews(payload),
});

export default connect(mstp, mdtp)(MainScreen);
