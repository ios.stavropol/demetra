import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import StaffView from '../../components/Profile/StaffView';
import {config} from '../../constants';

const ProfileScreen = ({userProfile, route}) => {
  const navigation = useNavigation();

  const [body, setBody] = React.useState(null);

  useEffect(() => {
    console.warn('userProfile: ', userProfile);
    let array = [];
    let keys = Object.keys(userProfile?.LEGAL_INFO);
    for (let i = 0; i < keys.length; i++) {
      array.push(
        <StaffView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
          }}
          navigation={navigation}
          data={userProfile?.LEGAL_INFO[keys[i]]}
          fulldata={userProfile}
          from={route?.name === 'Profile2' ? 'main' : route?.name}
        />,
      );
    }
    setBody(array);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  const renderView = (title, value, style) => {
    return (
      <View
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            height: Common.getLengthByIPhone7(70),
            borderBottomColor: colors.BORDER_COLOR,
            borderBottomWidth: 1,
          },
          style,
        ]}>
        <Text
          style={{
            marginTop: Common.getLengthByIPhone7(20),
            color: colors.GRAY_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          allowFontScaling={false}>
          {title}
        </Text>
        <Text
          style={{
            marginTop: 2,
            color: colors.TEXT_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(14),
          }}
          allowFontScaling={false}>
          {value}
        </Text>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        contentContainerStyle={{
          alignItems: 'center',
        }}>
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: Common.getLengthByIPhone7(20),
            marginBottom: Common.getLengthByIPhone7(10),
          }}>
          <View
            style={{
              width: Common.getLengthByIPhone7(80),
              height: Common.getLengthByIPhone7(80),
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: Common.getLengthByIPhone7(40),
              overflow: 'hidden',
              borderWidth: 2,
              borderColor: 'rgba(248, 170, 43, 0.3)',
            }}>
            {userProfile?.PERSONAL_PHOTO?.FILE?.SRC?.length ? (
              <Image
                source={{
                  uri: config.BASE_URL + userProfile?.PERSONAL_PHOTO?.FILE?.SRC,
                }}
                style={{
                  width: Common.getLengthByIPhone7(80),
                  height: Common.getLengthByIPhone7(80),
                  resizeMode: 'cover',
                }}
              />
            ) : (
              <Image
                source={require('./../../assets/ic-placeholder-user.png')}
                style={{
                  width: Common.getLengthByIPhone7(24),
                  height: Common.getLengthByIPhone7(24),
                  resizeMode: 'contain',
                }}
              />
            )}
          </View>
          <Text
            style={{
              width: Common.getLengthByIPhone7(250),
              marginLeft: Common.getLengthByIPhone7(14),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(14),
            }}
            allowFontScaling={false}>
            {userProfile?.FULL_NAME}
          </Text>
        </View>
        {body}
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            alignItems: 'center',
            marginTop: Common.getLengthByIPhone7(10),
            backgroundColor: colors.VIEW_COLOR,
            borderRadius: Common.getLengthByIPhone7(15),
          }}>
          {userProfile?.WORK_PHONE?.length
            ? renderView('Телефон', userProfile?.WORK_PHONE, {})
            : null}
          {userProfile?.PERSONAL_MOBILE?.length
            ? renderView('Мобильный', userProfile?.PERSONAL_MOBILE, {})
            : null}
          {userProfile?.EMAIL?.length
            ? renderView('E-mail', userProfile?.EMAIL, {})
            : null}
          {userProfile?.PERSONAL_BIRTHDAY?.length
            ? renderView('День рождения', userProfile?.PERSONAL_BIRTHDAY, {
                borderBottomWidth: 0,
              })
            : null}
        </View>
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            height: Common.getLengthByIPhone7(62),
            overflow: 'hidden',
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: Common.getLengthByIPhone7(10),
            marginBottom: Common.getLengthByIPhone7(10),
            backgroundColor: colors.VIEW_COLOR,
            borderRadius: Common.getLengthByIPhone7(15),
          }}>
          <LinearGradient
            colors={['#F7F7F7', '#F9F5EB']}
            start={{x: 0.0, y: 0.5}}
            end={{x: 1.0, y: 0.5}}
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
              height: Common.getLengthByIPhone7(62),
              position: 'absolute',
              left: 0,
              top: 0,
            }}
          />
          <Image
            source={require('./../../assets/ic-info.png')}
            style={{
              marginLeft: Common.getLengthByIPhone7(20),
              width: Common.getLengthByIPhone7(20),
              height: Common.getLengthByIPhone7(20),
              tintColor: 'rgba(78, 92, 107, 0.3)',
            }}
          />
          <Text
            style={{
              marginLeft: Common.getLengthByIPhone7(12),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
            }}
            allowFontScaling={false}>
            {`Для изменения персональных данных\nиспользуйте WEB-версию портала`}
          </Text>
        </View>
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({
  userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({});

export default connect(mstp, mdtp)(ProfileScreen);
