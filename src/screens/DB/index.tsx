import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import SearchView from '../../components/SearchView';
import CookieManager from '@react-native-cookies/cookies';
import {config} from '../../constants';

const images = [
  require('./../../assets/ic-db1.png'),
  require('./../../assets/ic-db2.png'),
  require('./../../assets/ic-db3.png'),
];

let count = 0;

const DBScreen = ({getKnowledgebase, db, dbRaw, setDB, accessToken}) => {
  const navigation = useNavigation();

  const [body, setBody] = React.useState(null);

  useEffect(() => {
    CookieManager.set(
      config.BASE_URL,
      {
        name: 'X-SOURCE',
        value: 'mobile',
        path: '/',
      },
      true,
    ).then(done => {});
    CookieManager.set(
      config.BASE_URL,
      {
        name: 'PHPSESSID',
        value: accessToken,
        path: '/',
      },
      true,
    ).then(done => {});
    getKnowledgebase()
      .then(() => {})
      .catch(err => {});
  }, []);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < db?.length; i++) {
      array.push(
        renderView(
          db[i].TITLE,
          images[i % 3],
          db[i].URL,
          {
            marginTop: Common.getLengthByIPhone7(10),
          },
          () => {
            navigation.navigate('Browser', {data: db[i].URL});
          },
        ),
      );
    }
    setBody(array);
  }, [db]);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        // setTimeout(() => {
        //   count = 0;
        // }, 2000);

        // if (count === 0) {
        //   count++;
        // } else if (count === 1) {
        //   BackHandler.exitApp();
        // }
        navigation.navigate('Main');
        return true;
      });
    }, []),
  );

  const renderView = (title, icon, code, style, action) => {
    return (
      <TouchableOpacity
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            height: Common.getLengthByIPhone7(60),
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          },
          style,
        ]}
        onPress={() => {
          if (action) {
            action();
          }
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Image
            source={icon}
            style={{
              width: Common.getLengthByIPhone7(60),
              height: Common.getLengthByIPhone7(60),
              resizeMode: 'contain',
            }}
          />
          <Text
            style={{
              marginLeft: Common.getLengthByIPhone7(13),
              width: Common.getLengthByIPhone7(220),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: '700',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(14),
            }}
            allowFontScaling={false}>
            {title}
          </Text>
        </View>
        <Image
          source={require('./../../assets/ic-arrow-right.png')}
          style={{
            width: Common.getLengthByIPhone7(6),
            height: Common.getLengthByIPhone7(12),
            resizeMode: 'contain',
            tintColor: colors.GRAY_COLOR,
            marginRight: Common.getLengthByIPhone7(13),
          }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <SearchView
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
        onSearch={text => {
          if (text?.length) {
            let array = dbRaw;
            array = array.filter(a => {
              return (
                a.TITLE.toLocaleLowerCase().indexOf(
                  text.toLocaleLowerCase(),
                ) !== -1
              );
            });
            setDB(array);
          } else {
            setDB(dbRaw);
          }
        }}
      />
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        contentContainerStyle={{
          alignItems: 'center',
        }}>
        {body}
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({
  db: state.user.db,
  dbRaw: state.user.dbRaw,
  accessToken: state.user.accessToken,
});

const mdtp = (dispatch: Dispatch) => ({
  getKnowledgebase: () => dispatch.user.getKnowledgebase(),
  setDB: payload => dispatch.user.setDB(payload),
});

export default connect(mstp, mdtp)(DBScreen);
