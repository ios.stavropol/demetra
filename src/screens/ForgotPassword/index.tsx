import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Alert,
  TouchableOpacity,
  Text,
  StatusBar,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import TextView from '../../components/Login/TextView';
import YellowButton from '../../components/YellowButton';
import {config} from '../../constants';

const ForgotPasswordScreen = ({restorePassword}) => {
  const navigation = useNavigation();

  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [disabled, setDisabled] = React.useState(false);
  const [button, setButton] = React.useState(null);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  useEffect(() => {
    console.warn('email: ', email, ' password: ', password);
    if (email?.length && password?.length) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [email, password]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <Text
        style={{
          marginTop: Common.getLengthByIPhone7(50),
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
          color: colors.TEXT_COLOR,
          fontFamily: 'OpenSans-Regular',
          fontWeight: 'normal',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(12),
        }}
        allowFontScaling={false}>
        Письмо для смены пароля будет выслано Вам на E-mail.
      </Text>
      <TextView
        title={'E-mail'}
        keyboardType={'email-address'}
        onChange={text => {
          setEmail(text);
        }}
        style={{
          marginTop: Common.getLengthByIPhone7(62),
        }}
      />
      <YellowButton
        title={'Выслать письмо'}
        disabled={false}
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
        onPress={() => {
          if (email?.length) {
            restorePassword(email)
              .then(() => {
                Alert.alert(config.APP_NAME, 'Письмо выслано Вам на почту!');
              })
              .catch(err => {});
          } else {
            Alert.alert(config.APP_NAME, 'Введите почту!');
          }
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  restorePassword: payload => dispatch.user.restorePassword(payload),
});

export default connect(mstp, mdtp)(ForgotPasswordScreen);
