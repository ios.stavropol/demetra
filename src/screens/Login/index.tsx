import React, {useCallback, useEffect, useRef} from 'react';
import {
  Platform,
  KeyboardAvoidingView,
  ScrollView,
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  Alert,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import TextView from '../../components/Login/TextView';
import SecurityView from '../../components/Login/SecurityView';
import YellowButton from '../../components/YellowButton';
import {config} from '../../constants';
import base64 from 'react-native-base64';

const LoginScreen = ({login, userProfile, getProfile, setBearer}) => {
  const navigation = useNavigation();

  const [email, setEmail] = React.useState('');
  const [error, setError] = React.useState(false);
  const [password, setPassword] = React.useState('');
  const [disabled, setDisabled] = React.useState(true);

  useEffect(() => {
    API.clearToken();
    StorageHelper.getData('email').then(res => {
      if (res?.length) {
        setEmail(res);
      }
    });
    StorageHelper.getData('password').then(res => {
      if (res?.length) {
        setPassword(res);
      }
    });
  }, []);

  useEffect(() => {
    if (email?.length && password?.length) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [email, password]);

  useFocusEffect(
    React.useCallback(() => {
      StorageHelper.getData('email').then(res => {
        if (res?.length) {
          setEmail(res);
        }
      });
      StorageHelper.getData('password').then(res => {
        if (res?.length) {
          setPassword(res);
        }
      });
      BackHandler.addEventListener('hardwareBackPress', function () {
        BackHandler.exitApp();
        return true;
      });
    }, []),
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{
          flex: 1,
          backgroundColor: colors.BACKGROUND_COLOR,
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
        <ScrollView
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
          }}
          contentContainerStyle={{
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
          <View
            style={{
              width: Common.getLengthByIPhone7(0),
              flex: 1,
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}>
            <Image
              source={require('./../../assets/ic-logo.png')}
              style={{
                width: Common.getLengthByIPhone7(101),
                height: Common.getLengthByIPhone7(120),
                marginTop: Common.getLengthByIPhone7(50),
              }}
            />
            <TextView
              title={'E-mail'}
              keyboardType={'email-address'}
              value={email}
              textContentType={'emailAddress'}
              bold
              onChange={text => {
                setEmail(text);
              }}
              style={{
                marginTop: Common.getLengthByIPhone7(50),
              }}
            />
            <SecurityView
              title={'Пароль'}
              value={password}
              textContentType={'password'}
              bold
              onChange={text => {
                setPassword(text);
              }}
              style={{
                marginTop: Common.getLengthByIPhone7(10),
              }}
            />
            <Text
              style={{
                marginTop: 2,
                width:
                  Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
                color: colors.RED_COLOR,
                fontFamily: 'OpenSans-Regular',
                fontWeight: 'normal',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(8),
                opacity: error ? 1 : 0,
              }}
              numberOfLines={1}
              allowFontScaling={false}>
              Некорректный пароль
            </Text>
            <YellowButton
              title={'Войти'}
              disabled={disabled}
              style={{
                marginTop: Common.getLengthByIPhone7(14),
                // opacity: disabled ? 0.4 : 1,
              }}
              onPress={() => {
                if (!disabled) {
                  API.clearToken();
                  if (email === '70@testtest.ru') {
                    config.BASE_URL = 'https://demetra.dev-vps.ru';
                  } else {
                    config.BASE_URL = 'https://portal.dholding.ru';
                  }
                  login(base64.encode(email + ':' + password))
                    .then(res => {
                      StorageHelper.saveData('email', email);
                      StorageHelper.saveData('password', password);
                      setError(false);
                      setBearer(base64.encode(email + ':' + password));
                      StorageHelper.saveData(
                        'bearer',
                        base64.encode(email + ':' + password),
                      );
                      if (res.result === 'ok') {
                        StorageHelper.saveData('token', res.sessid);
                        getProfile()
                          .then(() => {
                            navigation.navigate('LoginIn');
                          })
                          .catch(err => {});
                      } else {
                        navigation.navigate('OTP');
                      }
                    })
                    .catch(err => {
                      setError(true);
                    });
                } else {
                  Alert.alert(config.APP_NAME, 'Введите почту и пароль!');
                }
              }}
            />
            <TouchableOpacity
              style={{
                marginTop: Common.getLengthByIPhone7(13),
              }}
              onPress={() => {
                navigation.navigate('ForgotPassword');
              }}>
              <Text
                style={{
                  width:
                    Common.getLengthByIPhone7(0) -
                    Common.getLengthByIPhone7(60),
                  color: colors.GRAY_COLOR,
                  fontFamily: 'OpenSans-SemiBold',
                  fontWeight: 'normal',
                  textAlign: 'center',
                  fontSize: Common.getLengthByIPhone7(12),
                  textDecorationLine: 'underline',
                }}
                numberOfLines={1}
                allowFontScaling={false}>
                Забыли пароль?
              </Text>
            </TouchableOpacity>
            <Text
              style={{
                color: colors.GRAY_COLOR,
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: 'normal',
                textAlign: 'center',
                fontSize: Common.getLengthByIPhone7(10),
                marginTop: Common.getLengthByIPhone7(30),
              }}
              numberOfLines={1}
              allowFontScaling={false}>
              Версия {config.VERSION}
            </Text>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

const mstp = (state: RootState) => ({
  userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
  login: payload => dispatch.user.login(payload),
  setBearer: payload => dispatch.user.setBearer(payload),
  getProfile: () => dispatch.user.getProfile(),
});

export default connect(mstp, mdtp)(LoginScreen);
