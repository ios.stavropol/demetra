import React, {useCallback, useEffect, useRef} from 'react';
import {
  Platform,
  View,
  BackHandler,
  Alert,
  Text,
  ScrollView,
  Linking,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';
import {config} from '../../constants';
import RenderHtml from 'react-native-render-html';

const NewsPageScreen = ({route, getNewsById}) => {
  const navigation = useNavigation();

  const [data, setData] = React.useState(null);
  const [body, setBody] = React.useState(null);

  useEffect(() => {
    console.warn(route);
    getNewsById(route?.params?.data?.ID)
      .then(res => {
        setData(res);
      })
      .catch(err => {});
  }, []);

  const openURL = url => {
    if (url.indexOf(config.BASE_URL) !== -1) {
      url = url.split(config.BASE_URL);
      url = url[1];
      if (route.name === 'NewsPage2') {
        navigation.navigate('Browser2', {data: url});
      } else if (route.name === 'NewsPage3') {
        navigation.navigate('Browser3', {data: url});
      } else if (route.name === 'NewsPage') {
        navigation.navigate('Browser4', {data: url});
      }
      return;
    }
    Linking.openURL(url);
  };

  useEffect(() => {
    if (data) {
      setBody(
        <ScrollView
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
          }}
          contentContainerStyle={{
            alignItems: 'center',
          }}>
          <Text
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              marginTop: Common.getLengthByIPhone7(20),
              color: colors.GRAY_COLOR,
              fontFamily: 'OpenSans-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(10),
            }}
            numberOfLines={1}
            allowFontScaling={false}>
            {data?.LOG_DATE}
          </Text>
          <Text
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              marginTop: Common.getLengthByIPhone7(15),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: '700',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(14),
            }}
            allowFontScaling={false}>
            {data?.TITLE}
          </Text>
          <RenderHtml
            baseStyle={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40),
              marginTop: Common.getLengthByIPhone7(15),
            }}
            contentWidth={
              Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(40)
            }
            source={{
              baseUrl: config.BASE_URL,
              html: data?.MESSAGE,
            }}
            tagsStyles={{
              body: {
                whiteSpace: 'normal',
                color: 'black',
              },
            }}
            renderersProps={{
              a: {
                onPress(event, url, htmlAttribs, target) {
                  console.warn('url: ', url);
                  console.warn('route: ', route);
                  if (url.indexOf(config.BASE_URL) !== -1) {
                    if (url.indexOf('personal/user/') !== -1) {
                      let ids = url.split('personal/user/');
                      if (ids?.length === 2) {
                        ids = ids[1].split('/');
                        console.warn(ids);
                        if (ids?.length === 2 && ids[1]?.length === 0) {
                          if (route.name === 'NewsPage') {
                            navigation.navigate('AlienProfile4', {
                              data: {
                                ID: ids[0],
                              },
                              from: 'AlienProfile4',
                            });
                          } else if (route.name === 'NewsPage2') {
                            navigation.navigate('AlienProfile3', {
                              data: {
                                ID: ids[0],
                              },
                              from: 'AlienProfile3',
                            });
                          } else {
                            navigation.navigate('AlienProfile2', {
                              data: {
                                ID: ids[0],
                              },
                              from: 'AlienProfile2',
                            });
                          }
                        } else {
                          openURL(url);
                        }
                      } else {
                        openURL(url);
                      }
                    } else if (url.indexOf('/knowledge/') !== -1) {
                      let ids = url.split('/knowledge/');
                      if (ids?.length === 2) {
                        ids = '/mobile/knowledge/' + ids[1];
                        if (route.name === 'NewsPage2') {
                          navigation.navigate('Browser2', {data: ids});
                        } else if (route.name === 'NewsPage3') {
                          navigation.navigate('Browser3', {data: ids});
                        } else {
                          navigation.navigate('Browser4', {data: ids});
                        }
                      }
                    } else {
                      openURL(url);
                    }
                  } else {
                    openURL(url);
                  }
                },
              },
            }}
          />
        </ScrollView>,
      );
    }
  }, [data]);

  BackHandler.addEventListener('hardwareBackPress', function () {
    navigation.goBack();
    return true;
  });

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      {body}
    </View>
  );
};

const mstp = (state: RootState) => ({
  users: state.user.users,
});

const mdtp = (dispatch: Dispatch) => ({
  getNewsById: payload => dispatch.user.getNewsById(payload),
});

export default connect(mstp, mdtp)(NewsPageScreen);
