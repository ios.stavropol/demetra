import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  ActivityIndicator,
  Text,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import SearchView from '../../components/SearchView';
import UserView from '../../components/Users/UserView';

const UnitScreen = ({route, getDepartmentEmployee}) => {
  const navigation = useNavigation();

  const [body, setBody] = React.useState(null);
  const [search, setSearch] = React.useState('');
  const [users, setUsers] = React.useState([]);
  const [users2, setUsers2] = React.useState([]);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  useEffect(() => {
    let title = route?.params?.data?.NAME;
    title = title.length >= 25 ? title.slice(0, 26) + '...' : title;
    navigation.setOptions({
      title: title,
    });
    console.warn(route?.params?.data);
    getDepartmentEmployee(route?.params?.data?.ID)
      .then(res => {
        setUsers(res);
        setUsers2(res);
      })
      .catch(err => {});
  }, []);

  useEffect(() => {
    if (search.length) {
      let array = users.filter(a => {
        return (
          a.FULL_NAME.toLocaleLowerCase().indexOf(
            search.toLocaleLowerCase(),
          ) !== -1
        );
      });
      setUsers2(array);
    } else {
      setUsers2(users);
    }
  }, [search]);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < users2?.length; i++) {
      array.push(
        <UserView
          chief={route?.params?.data?.UF_HEAD}
          style={{
            marginTop:
              i === 0
                ? Common.getLengthByIPhone7(20)
                : Common.getLengthByIPhone7(10),
            marginBottom:
              i + 1 === users2?.length ? Common.getLengthByIPhone7(10) : 0,
          }}
          data={users2[i]}
          onClick={() => {
            navigation.navigate('AlienProfile', {data: users2[i]});
          }}
        />,
      );
    }
    setBody(array);
  }, [users2]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <SearchView
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
        value={search}
        onSearch={text => {
          setSearch(text);
        }}
      />
      <View
        style={{
          borderBottomColor: 'rgba(202, 206, 211, 0.3)',
          borderBottomWidth: 1,
          width: Common.getLengthByIPhone7(0),
          marginTop: Common.getLengthByIPhone7(20),
        }}
      />
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        contentContainerStyle={{
          alignItems: 'center',
        }}>
        {body}
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  getDepartmentEmployee: payload =>
    dispatch.user.getDepartmentEmployee(payload),
});

export default connect(mstp, mdtp)(UnitScreen);
