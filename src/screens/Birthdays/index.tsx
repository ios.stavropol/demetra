import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  ActivityIndicator,
  Text,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import SearchView from '../../components/SearchView';
import BirthdayView from '../../components/Users/BirthdayView';

const BirthdaysScreen = ({birthdays, route}) => {
  const navigation = useNavigation();

  const [body, setBody] = React.useState(null);
  const [search, setSearch] = React.useState('');
  const [objects, setObjects] = React.useState([]);

  const [pageNumber, setPageNumber] = React.useState(0);
  const [rowCount, setRowCount] = React.useState(10);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  useEffect(() => {
    let array = [];
    for (let i = 0; i < rowCount; i++) {
      array.push(birthdays[i]);
    }
    setObjects(array);
  }, []);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < Math.min(rowCount, birthdays?.length); i++) {
      array.push(birthdays[i]);
    }
    setObjects(array);
  }, [rowCount]);

  useEffect(() => {
    if (search?.length) {
      let array = birthdays.filter(a => {
        return (
          a.FULL_NAME.toLocaleLowerCase().indexOf(
            search.toLocaleLowerCase(),
          ) !== -1
        );
      });
      setObjects(array);
    } else {
      let array = [];
      for (let i = 0; i < rowCount; i++) {
        array.push(birthdays[i]);
      }
      setObjects(array);
    }
  }, [search]);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < objects?.length; i++) {
      array.push(
        <BirthdayView
          style={{
            marginTop:
              i === 0
                ? Common.getLengthByIPhone7(20)
                : Common.getLengthByIPhone7(10),
            marginBottom:
              i + 1 === objects?.length ? Common.getLengthByIPhone7(10) : 0,
          }}
          data={objects[i]}
          onClick={() => {
            console.warn(route);
            if (route.name === 'Birthdays') {
              navigation.navigate('AlienProfile3', {data: {ID: objects[i].ID}});
            } else {
              navigation.navigate('AlienProfile', {data: {ID: objects[i].ID}});
            }
          }}
        />,
      );
    }
    setBody(array);
  }, [objects]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <SearchView
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
        value={search}
        onSearch={text => {
          setSearch(text);
        }}
      />
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        contentContainerStyle={{
          alignItems: 'center',
        }}
        onScroll={event => {
          if (
            event.nativeEvent.contentSize.height - 300 <
            event.nativeEvent.layoutMeasurement.height +
              event.nativeEvent.contentOffset.y
          ) {
            if (search?.length === 0) {
              let obj = JSON.parse(JSON.stringify(rowCount));
              obj = obj + 10;
              setRowCount(obj);
            }
          }
        }}>
        {body}
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({
  users: state.user.users,
  birthdays: state.user.birthdays,
  structure: state.user.structure,
});

const mdtp = (dispatch: Dispatch) => ({
  getUsers: () => dispatch.user.getUsers(),
  setUsers: payload => dispatch.user.setUsers(payload),
});

export default connect(mstp, mdtp)(BirthdaysScreen);
