import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  ScrollView,
  RefreshControl,
  Dimensions,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import NewsView from '../../components/News/NewsView';
import ImportantView from '../../components/News/ImportantView';
import TopButtonView from '../../components/News/TopButtonView';

let count = 0;

const NewsScreen = ({
  route,
  changeTab,
  getImportant,
  importants,
  getNews,
  news,
}) => {
  const navigation = useNavigation();

  const [tab, setTab] = React.useState(0);
  const [body, setBody] = React.useState(null);
  const [body2, setBody2] = React.useState(null);
  const [importantPageNumber, setImportantPageNumber] = React.useState(0);
  const [importantRowCount, setImportantRowCount] = React.useState(10);
  const [newsPageNumber, setNewsPageNumber] = React.useState(0);
  const [newsRowCount, setNewsRowCount] = React.useState(10);
  const [loading, setLoading] = React.useState(false);

  const scroll = useRef(null);

  useEffect(() => {
    loadFunc();
  }, []);

  useEffect(() => {
    if (tab === 0) {
      scroll?.current?.scrollTo({x: 0, animated: true});
    } else if (tab === 1) {
      scroll?.current.scrollTo({
        x: Common.getLengthByIPhone7(0),
        animated: true,
      });
    } else if (tab === 2) {
      scroll.current.scrollTo({
        x: 2 * Common.getLengthByIPhone7(0),
        animated: true,
      });
    }
  }, [tab]);

  const loadFunc = () => {
    getImportant({
      pageNumber: 1,
      rowCount: importantRowCount,
    })
      .then(() => {})
      .catch(err => {});
    getNews({
      pageNumber: 1,
      rowCount: newsRowCount,
    })
      .then(() => {})
      .catch(err => {});
  };

  useEffect(() => {
    setTab(changeTab);
    setNewsPageNumber(0);
    setImportantPageNumber(0);
    if (scroll?.current) {
      setTimeout(() => {
        scroll?.current?.scrollTo({y: 0, x: 0, animated: true});
      }, 100);
    }
  }, [changeTab]);

  useEffect(() => {
    if (tab === 1) {
      getImportant({
        pageNumber: importantPageNumber,
        rowCount: importantRowCount,
      })
        .then(() => {})
        .catch(err => {});
    }
  }, [importantPageNumber]);

  useEffect(() => {
    if (tab === 0) {
      getNews({
        pageNumber: newsPageNumber,
        rowCount: newsRowCount,
      })
        .then(() => {})
        .catch(err => {});
    }
  }, [newsPageNumber]);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        // setTimeout(() => {
        //   count = 0;
        // }, 2000);

        // if (count === 0) {
        //   count++;
        // } else if (count === 1) {
        //   BackHandler.exitApp();
        // }
        navigation.navigate('Main');
        return true;
      });
    }, []),
  );

  useEffect(() => {
    let array = [];

    for (let i = 0; i < news.length; i++) {
      array.push(
        <NewsView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
            marginBottom:
              i + 1 === news.length ? Common.getLengthByIPhone7(10) : 0,
          }}
          data={news[i]}
          action={() => {
            navigation.navigate('NewsPage', {data: news[i]});
          }}
        />,
      );
    }
    setBody(array);
    array = [];
    for (let i = 0; i < importants.length; i++) {
      array.push(
        <ImportantView
          style={{
            marginTop: Common.getLengthByIPhone7(10),
            marginBottom:
              i + 1 === importants.length ? Common.getLengthByIPhone7(10) : 0,
          }}
          data={importants[i]}
          action={() => {
            navigation.navigate('ImportantPage', {data: importants[i]});
          }}
        />,
      );
    }
    setBody2(array);
  }, [news, importants]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <TopButtonView
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
        action={obj => {
          setTab(obj);
        }}
        data={tab}
      />
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        pagingEnabled
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        ref={el => (scroll.current = el)}
        onMomentumScrollEnd={event => {
          let page = Math.round(
            parseFloat(
              event.nativeEvent.contentOffset.x /
                Dimensions.get('window').width,
            ),
          );
          // console.warn(page);
          setTab(page);
        }}
        contentContainerStyle={{
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
        <ScrollView
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
          }}
          ref={scroll}
          contentContainerStyle={{
            alignItems: 'center',
          }}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={() => {
                loadFunc();
              }}
            />
          }
          onScroll={event => {
            if (
              event.nativeEvent.contentSize.height - 300 <
              event.nativeEvent.layoutMeasurement.height +
                event.nativeEvent.contentOffset.y
            ) {
              let obj = JSON.parse(JSON.stringify(newsPageNumber));
              obj++;
              setNewsPageNumber(obj);
            }
          }}>
          {body}
        </ScrollView>
        <ScrollView
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
          }}
          ref={scroll}
          contentContainerStyle={{
            alignItems: 'center',
          }}
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={() => {
                loadFunc();
              }}
            />
          }
          onScroll={event => {
            if (
              event.nativeEvent.contentSize.height - 300 <
              event.nativeEvent.layoutMeasurement.height +
                event.nativeEvent.contentOffset.y
            ) {
              let obj = JSON.parse(JSON.stringify(importantPageNumber));
              obj++;
              setImportantPageNumber(obj);
            }
          }}>
          {body2}
        </ScrollView>
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({
  importants: state.user.importants,
  news: state.user.news,
  changeTab: state.buttons.changeTab,
});

const mdtp = (dispatch: Dispatch) => ({
  getImportant: payload => dispatch.user.getImportant(payload),
  getNews: payload => dispatch.user.getNews(payload),
});

export default connect(mstp, mdtp)(NewsScreen);
