import React, { useCallback, useEffect, useRef } from 'react';
import { BackHandler, View, FlatList, TouchableOpacity, Text, StatusBar } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import UserView from '../../components/Main/UserView';

const UserScreen = ({currentUser}) => {

	const navigation = useNavigation();

	useEffect(() => {
		console.warn(currentUser);
	}, []);

	useFocusEffect(
		React.useCallback(() => {
			BackHandler.addEventListener('hardwareBackPress', function () {
				navigation.goBack();
				return true;
			});
		}, [])
	);

	return (
		<View style={{
			flex: 1,
			backgroundColor: colors.BACKGROUND_COLOR,
			alignItems: 'center',
			justifyContent: 'flex-start',
		}}>
			<Text style={{
				color: colors.TEXT_COLOR,
				fontFamily: 'Gilroy-Regular',
				fontWeight: 'normal',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(20),
				marginTop: Common.getLengthByIPhone7(20),
			}}
			numberOfLines={1}
			allowFontScaling={false}>
				{currentUser.name}
			</Text>
			<Text style={{
				color: colors.TEXT_COLOR,
				fontFamily: 'Gilroy-Regular',
				fontWeight: 'normal',
				textAlign: 'left',
				fontSize: Common.getLengthByIPhone7(15),
				marginTop: Common.getLengthByIPhone7(10),
			}}
			allowFontScaling={false}>
				{currentUser.email}
			</Text>
		</View>
	);
};

const mstp = (state: RootState) => ({
	currentUser: state.user.currentUser,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(UserScreen);
