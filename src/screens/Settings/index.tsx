import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {config} from '../../constants';
import {Switch} from 'react-native-switch';

const SettingsScreen = ({
  userProfile,
  getProfile,
  setImportantNotification,
  setNewsNotification,
}) => {
  const navigation = useNavigation();

  const [news, setNews] = React.useState(false);
  const [messages, setMessages] = React.useState(false);
  const [body, setBody] = React.useState(null);

  useEffect(() => {
    if (userProfile?.UF_NEWS_PUSH_NOTIFICATION == 1) {
      setNews(true);
    } else {
      setNews(false);
    }

    if (userProfile?.UF_IMPORTANT_PUSH_NOTIFICATION == 1) {
      setMessages(true);
    } else {
      setMessages(false);
    }
  }, []);

  useEffect(() => {
    let array = [];
    array.push(
      renderView(
        'Новости',
        news,
        {
          marginTop: Common.getLengthByIPhone7(24),
        },
        val => {
          setNews(val);
          setNewsNotification(val)
            .then(() => {
              getProfile();
            })
            .catch(err => {});
        },
      ),
    );
    array.push(
      renderView(
        'Важные сообщения',
        messages,
        {
          borderBottomWidth: 0,
        },
        val => {
          setMessages(val);
          setImportantNotification(val)
            .then(() => {
              getProfile();
            })
            .catch(err => {});
        },
      ),
    );
    setBody(array);
  }, [news, messages]);

  useFocusEffect(
    React.useCallback(() => {
      console.warn(userProfile);
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  const renderView = (title, value, style, action) => {
    return (
      <View
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            height: Common.getLengthByIPhone7(35),
            borderBottomColor: colors.GRAY_COLOR,
            borderBottomWidth: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          },
          style,
        ]}>
        <Text
          style={{
            color: colors.TEXT_COLOR,
            fontFamily: 'OpenSans-Regular',
            fontWeight: 'normal',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(14),
          }}
          allowFontScaling={false}>
          {title}
        </Text>
        <View
          style={{
            width: Common.getLengthByIPhone7(42),
            height: Common.getLengthByIPhone7(24),
            borderRadius: Common.getLengthByIPhone7(24),
            alignItems: 'center',
            justifyContent: 'center',
            overflow: 'hidden',
            borderColor: value ? colors.YELLOW_ACTIVE_COLOR : colors.GRAY_COLOR,
            borderWidth: 2,
          }}>
          <Switch
            value={value}
            onValueChange={val => {
              if (action) {
                action(val);
              }
            }}
            activeText={'On'}
            inActiveText={'Off'}
            circleSize={Common.getLengthByIPhone7(16)}
            barHeight={Common.getLengthByIPhone7(24)}
            circleBorderWidth={0}
            backgroundActive={'white'}
            backgroundInactive={'white'}
            circleActiveColor={colors.YELLOW_ACTIVE_COLOR}
            circleInActiveColor={colors.GRAY_COLOR}
            changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
            innerCircleStyle={{alignItems: 'center', justifyContent: 'center'}} // style for inner animated circle for what you (may) be rendering inside the circle
            renderActiveText={false}
            renderInActiveText={false}
            switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
            switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
            switchWidthMultiplier={2.5}
            switchBorderRadius={Common.getLengthByIPhone7(24)}
          />
        </View>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <Text
        style={{
          width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
          marginTop: Common.getLengthByIPhone7(38),
          color: colors.TEXT_COLOR,
          fontFamily: 'OpenSans-Bold',
          fontWeight: '700',
          textAlign: 'left',
          fontSize: Common.getLengthByIPhone7(12),
        }}
        allowFontScaling={false}>
        {`Настройки PUSH-уведомлений`.toLocaleUpperCase()}
      </Text>
      {body}
    </View>
  );
};

const mstp = (state: RootState) => ({
  userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
  setImportantNotification: payload =>
    dispatch.user.setImportantNotification(payload),
  setNewsNotification: payload => dispatch.user.setNewsNotification(payload),
  getProfile: () => dispatch.user.getProfile(),
});

export default connect(mstp, mdtp)(SettingsScreen);
