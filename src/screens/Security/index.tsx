import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Alert,
  KeyboardAvoidingView,
  Text,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import PasswordView from '../../components/Security/PasswordView';
import YellowButton from '../../components/YellowButton';
import {config} from '../../constants';

const SecurityScreen = ({changePassword}) => {
  const navigation = useNavigation();

  const [old, setOld] = React.useState('');
  const [newPass, setNewPass] = React.useState('');
  const [newPass2, setNewPass2] = React.useState('');
  const [disabled, setDisabled] = React.useState(true);

  const newPassRef = useRef(null);
  const newPass2Ref = useRef(null);

  useEffect(() => {
    if (old?.length && newPass?.length && newPass2?.length) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [old, newPass, newPass2]);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        return true;
      });
    }, []),
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={{
          flex: 1,
          backgroundColor: colors.BACKGROUND_COLOR,
          alignItems: 'center',
          justifyContent: 'flex-start',
        }}>
        <ScrollView
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
          }}
          contentContainerStyle={{
            alignItems: 'center',
            justifyContent: 'flex-start',
          }}>
          <Text
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
              marginTop: Common.getLengthByIPhone7(38),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: '700',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
            }}
            allowFontScaling={false}>
            {`Изменить пароль`.toUpperCase()}
          </Text>
          <Text
            style={{
              width:
                Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
              marginTop: Common.getLengthByIPhone7(10),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Regular',
              fontWeight: 'normal',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(12),
            }}
            allowFontScaling={false}>
            {`Вы можете использовать патинские буквы, цифры и специальные символы ! @ # $ % & * ( ) _ - + в любом их сочетании.\n\nПароль должен быть не короче 6 символов.`}
          </Text>
          <PasswordView
            style={{
              marginTop: Common.getLengthByIPhone7(30),
            }}
            title={'Текущий пароль'}
            onSubmitEditing={() => {
              if (newPassRef?.current) {
                newPassRef.current.onFocus();
              }
            }}
            onChange={text => {
              setOld(text);
            }}
          />
          <PasswordView
            style={{
              marginTop: Common.getLengthByIPhone7(20),
            }}
            ref={newPassRef}
            title={'Введите новый пароль'}
            onChange={text => {
              setNewPass(text);
            }}
          />
          <PasswordView
            style={{
              marginTop: Common.getLengthByIPhone7(20),
            }}
            title={'Повторите пароль'}
            onChange={text => {
              setNewPass2(text);
            }}
          />
          <YellowButton
            title={'Сохранить'}
            disabled={disabled}
            style={{
              marginTop: Common.getLengthByIPhone7(15),
              // opacity: disabled ? 0.4 : 1,
            }}
            onPress={() => {
              if (!disabled) {
                if (newPass !== newPass2) {
                  Alert.alert(config.APP_NAME, 'Новые пароли не равны!');
                  return;
                }

                changePassword(newPass)
                  .then(() => {
                    Alert.alert(config.APP_NAME, 'Пароль изменен', [
                      {
                        text: 'OK',
                        onPress: () => {
                          navigation.goBack();
                        },
                      },
                    ]);
                  })
                  .catch(err => {
                    Alert.alert(config.APP_NAME, err);
                  });
              } else {
                Alert.alert(config.APP_NAME, 'Заполните все поля!');
              }
            }}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  changePassword: payload => dispatch.user.changePassword(payload),
});

export default connect(mstp, mdtp)(SecurityScreen);
