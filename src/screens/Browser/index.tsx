import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  Linking,
  Alert,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import CookieManager from '@react-native-cookies/cookies';
import {WebView} from 'react-native-webview';
import {config} from '../../constants';

const INJECTEDJAVASCRIPT = `const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=0.6, maximum-scale=0.6, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta);`;

const BrowserScreen = ({route, accessToken, bearer}) => {
  const navigation = useNavigation();

  useEffect(() => {
    console.warn('PHPSESSID=' + bearer + ';');
    CookieManager.getAll().then(cookies => {
      console.warn('CookieManager.getAll =>', cookies);
    });
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <WebView
        source={{
          uri: config.BASE_URL + route?.params?.data,
          headers: {
            Cookie: 'PHPSESSID=' + accessToken + ';path=/',
            // Authorization: 'Basic ' + bearer,
          },
        }}
        originWhitelist={['*']}
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        containerStyle={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        scalesPageToFit
        sharedProcess={true}
        processPool="MAIN"
        javaScriptEnabled={true}
        injectedJavaScript={INJECTEDJAVASCRIPT}
        domStorageEnabled={true}
        startInLoadingState
        onShouldStartLoadWithRequest={request => {
          console.warn(request.url);
          if (request.url.indexOf('company/personal/user/') !== -1) {
            let str = request.url.split('company/personal/user/');
            str = str[1].split('/');
            if (str?.length === 2 && str[1].length === 0) {
              navigation.navigate('AlienProfile5', {
                data: {ID: parseInt(str[0])},
                from: 'AlienProfile5',
              });
            } else if (str[1].indexOf('__bx_android_click_detect__') !== -1) {
              navigation.navigate('AlienProfile5', {
                data: {ID: parseInt(str[0])},
                from: 'AlienProfile5',
              });
            }
            return false;
          }
          // short circuit these
          if (
            !request.url ||
            request.url.startsWith('http') ||
            request.url.startsWith('/') ||
            request.url.startsWith('#') ||
            request.url.startsWith('javascript') ||
            request.url.startsWith('about:blank')
          ) {
            return true;
          }

          // blocked blobs
          if (request.url.startsWith('blob')) {
            Alert.alert('Link cannot be opened.');
            return false;
          }

          // list of schemas we will allow the webview
          // to open natively
          if (
            request.url.startsWith('tel:') ||
            request.url.startsWith('mailto:') ||
            request.url.startsWith('maps:') ||
            request.url.startsWith('geo:') ||
            request.url.startsWith('sms:')
          ) {
            Linking.openURL(request.url).catch(er => {
              Alert.alert('Failed to open Link: ' + er.message);
            });
            return false;
          }

          // let everything else to the webview
          return true;
        }}
        onError={syntheticEvent => {
          const {nativeEvent} = syntheticEvent;
          console.warn('WebView error: ', nativeEvent);
        }}
        onLoad={request => {
          console.warn('onLoad: ', request);
        }}
        onLoadStart={request => {
          console.warn('onLoadStart: ', request);
        }}
        onLoadEnd={request => {
          console.warn('onLoadEnd: ', request);
        }}
        onNavigationStateChange={newNavState => {
          console.warn('onNavigationStateChange: ', newNavState);
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({
  accessToken: state.user.accessToken,
  bearer: state.user.bearer,
});

const mdtp = (dispatch: Dispatch) => ({
  getKnowledgebase: () => dispatch.user.getKnowledgebase(),
});

export default connect(mstp, mdtp)(BrowserScreen);
