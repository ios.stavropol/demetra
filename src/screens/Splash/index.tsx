import React, {useCallback, useEffect, useRef} from 'react';
import {
  Platform,
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  StatusBar,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation} from '@react-navigation/native';
import CookieManager from '@react-native-cookies/cookies';
import {config} from '../../constants';
import {getUniqueId} from 'react-native-device-info';

const SplashScreen = ({getProfile, setDeviceId, setAccessToken, setBearer}) => {
  const navigation = useNavigation();

  useEffect(() => {
    // config.BASE_URL = 'https://portal.dholding.ru';
    getUniqueId().then(uniqueId => {
      setDeviceId(uniqueId);
      API.loadDeviceId(uniqueId);
    });

    StorageHelper.getData('url')
      .then(res => {
        if (res?.length) {
          config.BASE_URL = res;
        } else {
          config.BASE_URL = 'https://portal.dholding.ru';
        }
        getData();
      })
      .catch(() => {
        config.BASE_URL = 'https://portal.dholding.ru';
        getData();
      });
  }, []);

  const getData = () => {
    console.warn(config.BASE_URL);
    StorageHelper.getData('token')
      .then(token => {
        console.warn('token: ', token);
        if (token?.length) {
          API.loadToken(token);
          setTimeout(() => {
            getProfile()
              .then(() => {
                setAccessToken(token);
                navigation.navigate('LoginIn');
              })
              .catch(err => {
                StorageHelper.getData('bearer')
                  .then(bearer => {
                    setBearer(bearer);
                  })
                  .catch(err => {});
                // StorageHelper.saveData('token', '');
                navigation.navigate('Login');
              });
          }, 1000);
        } else {
          // StorageHelper.saveData('token', '');
          navigation.navigate('Login');
        }
      })
      .catch(err => {
        // StorageHelper.saveData('token', '');
        navigation.navigate('Login');
      });
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'space-between',
      }}>
      <ImageBackground
        source={require('./../../assets/splash.png')}
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({
  users: state.user.users,
});

const mdtp = (dispatch: Dispatch) => ({
  getProfile: () => dispatch.user.getProfile(),
  setAccessToken: payload => dispatch.user.setAccessToken(payload),
  setBearer: payload => dispatch.user.setBearer(payload),
  setDeviceId: payload => dispatch.user.setDeviceId(payload),
});

export default connect(mstp, mdtp)(SplashScreen);
