import React, {useCallback, useEffect, useRef} from 'react';
import {BackHandler, View, Image, Alert, Text, StatusBar} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import TextView from '../../components/Login/TextView';
import YellowButton from '../../components/YellowButton';
import {config} from '../../constants';

const OTPScreen = ({sendCode, token}) => {
  const navigation = useNavigation();

  const [code, setCode] = React.useState('');
  const [disabled, setDisabled] = React.useState(false);
  const [button, setButton] = React.useState(null);
  const textRef = React.useRef<TextView>(null);

  useEffect(() => {
    setButton(
      <YellowButton
        title={'Продолжить'}
        disabled={false}
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
      />,
    );
    if (textRef?.current) {
      textRef?.current?.blur();
      setTimeout(() => {
        textRef?.current?.focus();
      }, 500);
    }
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      if (textRef?.current) {
        textRef?.current?.blur();
        setTimeout(() => {
          textRef?.current?.focus();
        }, 500);
      }
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  useEffect(() => {
    if (code?.length === 6) {
      next();
    }
  }, [code]);

  const next = () => {
    if (code?.length === 6) {
      console.warn('code: ', code.length);
      sendCode(code)
        .then(() => {
          StorageHelper.saveData('token', token);
          setCode('');
          navigation.navigate('LoginIn');
        })
        .catch(err => {
          Alert.alert(config.APP_NAME, err);
        });
    } else {
      Alert.alert(config.APP_NAME, 'Введите код!');
    }
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <TextView
        title={'Введите код Bitrix OTP'}
        onChange={text => {
          setCode(text);
        }}
        ref={textRef}
        bold
        value={code}
        style={{
          marginTop: Common.getLengthByIPhone7(144),
        }}
        keyboardType={'number-pad'}
      />
      {/* <YellowButton
        title={'Продолжить'}
        disabled={disabled}
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
        onPress={() => {
          next();
        }}
      /> */}
    </View>
  );
};

const mstp = (state: RootState) => ({
  token: state.user.token,
});

const mdtp = (dispatch: Dispatch) => ({
  sendCode: payload => dispatch.user.sendCode(payload),
});

export default connect(mstp, mdtp)(OTPScreen);
