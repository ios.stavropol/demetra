import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  ScrollView,
  Linking,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import StaffView from '../../components/Profile/StaffView';
import {config} from '../../constants';

const months = {
  '01': 'января',
  '02': 'февраля',
  '03': 'марта',
  '04': 'апреля',
  '05': 'мая',
  '06': 'июня',
  '07': 'июля',
  '08': 'августа',
  '09': 'сентября',
  '10': 'октября',
  '11': 'ноября',
  '12': 'декабря',
};

const AlienProfileScreen = ({route, getUserById}) => {
  const navigation = useNavigation();

  const [body, setBody] = React.useState(null);
  const [data, setData] = React.useState(null);

  useEffect(() => {
    console.warn(route);
    getUserById(route?.params?.data?.ID)
      .then(res => {
        setData(res);
      })
      .catch(err => {});
  }, []);

  useEffect(() => {
    if (data) {
      let array = [];
      let keys = Object.keys(data?.LEGAL_INFO);
      for (let i = 0; i < keys.length; i++) {
        array.push(
          <StaffView
            style={{
              marginTop: Common.getLengthByIPhone7(10),
            }}
            data={data?.LEGAL_INFO[keys[i]]}
            fulldata={data}
            from={route?.params?.from}
            navigation={navigation}
          />,
        );
      }
      setBody(array);
    }
  }, [data]);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  const renderView = (title, value, style, action) => {
    return (
      <View
        style={[
          {
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
            height: Common.getLengthByIPhone7(70),
            borderBottomColor: colors.BORDER_COLOR,
            borderBottomWidth: 1,
          },
          style.viewStyle,
        ]}>
        <Text
          style={{
            marginTop: Common.getLengthByIPhone7(20),
            color: colors.GRAY_COLOR,
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            textAlign: 'left',
            fontSize: Common.getLengthByIPhone7(10),
          }}
          allowFontScaling={false}>
          {title}
        </Text>
        <TouchableOpacity
          style={{
            marginTop: 2,
          }}
          activeOpacity={1}
          onPress={() => {
            if (action) {
              action();
            }
          }}>
          <Text
            style={[
              {
                color: colors.TEXT_COLOR,
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: '600',
                textAlign: 'left',
                fontSize: Common.getLengthByIPhone7(14),
              },
              style.textStyle,
            ]}
            allowFontScaling={false}>
            {value}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <ScrollView
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
        }}
        contentContainerStyle={{
          alignItems: 'center',
        }}>
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: Common.getLengthByIPhone7(20),
            marginBottom: Common.getLengthByIPhone7(10),
          }}>
          <View
            style={{
              width: Common.getLengthByIPhone7(80),
              height: Common.getLengthByIPhone7(80),
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: Common.getLengthByIPhone7(40),
              overflow: 'hidden',
              borderWidth: 2,
              borderColor: 'rgba(248, 170, 43, 0.3)',
            }}>
            {data?.PERSONAL_PHOTO?.FILE?.SRC?.length ? (
              <Image
                source={{
                  uri: config.BASE_URL + data?.PERSONAL_PHOTO?.FILE?.SRC,
                }}
                style={{
                  width: Common.getLengthByIPhone7(80),
                  height: Common.getLengthByIPhone7(80),
                  resizeMode: 'cover',
                }}
              />
            ) : (
              <Image
                source={require('./../../assets/ic-placeholder-user.png')}
                style={{
                  width: Common.getLengthByIPhone7(24),
                  height: Common.getLengthByIPhone7(24),
                  resizeMode: 'contain',
                }}
              />
            )}
          </View>
          <Text
            style={{
              width: Common.getLengthByIPhone7(250),
              marginLeft: Common.getLengthByIPhone7(14),
              color: colors.TEXT_COLOR,
              fontFamily: 'OpenSans-Bold',
              fontWeight: 'bold',
              textAlign: 'left',
              fontSize: Common.getLengthByIPhone7(14),
            }}
            allowFontScaling={false}>
            {data?.FULL_NAME}
          </Text>
        </View>
        {body}
        <View
          style={{
            width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(20),
            alignItems: 'center',
            marginTop: Common.getLengthByIPhone7(10),
            backgroundColor: colors.VIEW_COLOR,
            borderRadius: Common.getLengthByIPhone7(15),
          }}>
          {data?.WORK_PHONE?.length
            ? renderView(
                'Телефон',
                data?.WORK_PHONE,
                {
                  textStyle: {
                    // textDecorationLine: 'underline',
                  },
                  viewStyle: {},
                },
                () => {
                  Linking.openURL('tel:' + data?.WORK_PHONE);
                },
              )
            : null}
          {data?.PERSONAL_MOBILE?.length
            ? renderView(
                'Мобильный',
                data?.PERSONAL_MOBILE,
                {
                  textStyle: {
                    // textDecorationLine: 'underline',
                  },
                  viewStyle: {},
                },
                () => {
                  Linking.openURL('tel:' + data?.PERSONAL_MOBILE);
                },
              )
            : null}
          {data?.EMAIL?.length
            ? renderView(
                'E-mail',
                data?.EMAIL,
                {
                  textStyle: {
                    // textDecorationLine: 'underline',
                  },
                  viewStyle: {},
                },
                () => {
                  Linking.openURL('mailto:' + data?.EMAIL);
                },
              )
            : null}
          {data?.PERSONAL_BIRTHDAY?.length
            ? renderView(
                'День рождения',
                data?.PERSONAL_BIRTHDAY.substring(0, 2) +
                  ' ' +
                  months[data?.PERSONAL_BIRTHDAY.substring(3, 5)],
                {
                  textStyle: {},
                  viewStyle: {
                    borderBottomWidth: 0,
                  },
                },
              )
            : null}
        </View>
      </ScrollView>
    </View>
  );
};

const mstp = (state: RootState) => ({
  users: state.user.users,
});

const mdtp = (dispatch: Dispatch) => ({
  getUserById: payload => dispatch.user.getUserById(payload),
});

export default connect(mstp, mdtp)(AlienProfileScreen);
