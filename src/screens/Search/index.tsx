import React, {useCallback, useState, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  TouchableOpacity,
  Text,
  StatusBar,
  ScrollView,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import SearchView from '../../components/SearchView';
import UserView from '../../components/Search/UserView';
import EventsView from '../../components/Search/EventsView';

const titles = {
  livefeed: 'Лента событий',
  task: 'Задачи',
  calendar: 'Календарь',
  user: 'Сотрудники',
};

let count = 0;

const SearchScreen = ({search}) => {
  const navigation = useNavigation();

  const [body, setBody] = React.useState(null);
  const [searchText, setSearchText] = React.useState('');
  const searchRef = React.useRef<SearchView>(null);

  useFocusEffect(
    React.useCallback(() => {
      if (searchRef?.current) {
        console.warn('useFocusEffect');
        searchRef?.current?.focus();
      }
      BackHandler.addEventListener('hardwareBackPress', function () {
        // setTimeout(() => {
        //   count = 0;
        // }, 2000);

        // if (count === 0) {
        //   count++;
        // } else if (count === 1) {
        //   BackHandler.exitApp();
        // }
        navigation.navigate('Main');
        return true;
      });
    }, []),
  );

  useEffect(() => {
    const handler = setTimeout(() => {
      if (searchText?.length > 0) {
        search(searchText)
          .then(res => {
            let keys = Object.keys(res);
            console.warn(keys);
            let array = [];
            let count = 0;
            for (let i = 0; i < keys.length; i++) {
              if (res[keys[i]]?.length) {
                if (keys[i] === 'livefeed') {
                  console.warn('res[keys[i]]: ', res[keys[i]]);
                  let news = [];
                  let important = [];

                  for (let z = 0; z < res[keys[i]]?.length; z++) {
                    if (res[keys[i]][z].EVENT_ID === 'blog_post_news') {
                      news.push(res[keys[i]][z]);
                      count++;
                    } else {
                      important.push(res[keys[i]][z]);
                      count++;
                    }
                  }

                  if (news.length) {
                    array.push(
                      <View
                        style={{
                          width: Common.getLengthByIPhone7(0),
                        }}>
                        <Text
                          style={{
                            marginTop: Common.getLengthByIPhone7(15),
                            marginBottom: Common.getLengthByIPhone7(5),
                            marginLeft: Common.getLengthByIPhone7(10),
                            color: colors.TEXT_COLOR,
                            fontFamily: 'OpenSans-SemiBold',
                            fontWeight: '600',
                            textAlign: 'left',
                            fontSize: Common.getLengthByIPhone7(14),
                          }}
                          allowFontScaling={false}>
                          Новости
                        </Text>
                      </View>,
                    );
                    array.push(
                      <EventsView
                        style={{}}
                        navigation={navigation}
                        data={news}
                      />,
                    );
                  }
                  if (important.length) {
                    array.push(
                      <View
                        style={{
                          width: Common.getLengthByIPhone7(0),
                        }}>
                        <Text
                          style={{
                            marginTop: Common.getLengthByIPhone7(15),
                            marginBottom: Common.getLengthByIPhone7(5),
                            marginLeft: Common.getLengthByIPhone7(10),
                            color: colors.TEXT_COLOR,
                            fontFamily: 'OpenSans-SemiBold',
                            fontWeight: '600',
                            textAlign: 'left',
                            fontSize: Common.getLengthByIPhone7(14),
                          }}
                          allowFontScaling={false}>
                          Важные сообщения
                        </Text>
                      </View>,
                    );
                    array.push(
                      <EventsView
                        style={{}}
                        navigation={navigation}
                        data={important}
                      />,
                    );
                  }
                } else {
                  if (keys[i] !== 'calendar' || keys[i] === 'task') {
                    array.push(
                      <View
                        style={{
                          width: Common.getLengthByIPhone7(0),
                        }}>
                        <Text
                          style={{
                            marginTop: Common.getLengthByIPhone7(15),
                            marginBottom: Common.getLengthByIPhone7(5),
                            marginLeft: Common.getLengthByIPhone7(10),
                            color: colors.TEXT_COLOR,
                            fontFamily: 'OpenSans-SemiBold',
                            fontWeight: '600',
                            textAlign: 'left',
                            fontSize: Common.getLengthByIPhone7(14),
                          }}
                          allowFontScaling={false}>
                          {titles[keys[i]]}
                        </Text>
                      </View>,
                    );
                  }

                  if (keys[i] === 'task') {
                    // array.push(<TaskView style={{}} data={res[keys[i]]} />);
                  } else if (keys[i] === 'calendar') {
                    // array.push(<CalendarView style={{}} data={res[keys[i]]} />);
                  } else if (keys[i] === 'user') {
                    array.push(
                      <UserView
                        style={{}}
                        navigation={navigation}
                        data={res[keys[i]]}
                      />,
                    );
                    count++;
                  } else if (keys[i] === 'livefeed') {
                    array.push(
                      <EventsView
                        style={{}}
                        navigation={navigation}
                        data={res[keys[i]]}
                      />,
                    );
                    count++;
                  }
                }
              }
            }
            if (count) {
              setBody(
                <ScrollView
                  style={{
                    width: Common.getLengthByIPhone7(0),
                    flex: 1,
                  }}
                  contentContainerStyle={{
                    alignItems: 'center',
                  }}>
                  {array}
                </ScrollView>,
              );
            } else {
              setBody(
                <View
                  style={{
                    width: Common.getLengthByIPhone7(0),
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{
                      width:
                        Common.getLengthByIPhone7(0) -
                        Common.getLengthByIPhone7(40),
                      color: colors.TEXT_COLOR,
                      fontFamily: 'OpenSans-SemiBold',
                      fontWeight: '600',
                      textAlign: 'center',
                      fontSize: Common.getLengthByIPhone7(18),
                    }}
                    allowFontScaling={false}>
                    Результаты по заданному критерию отсутствуют
                  </Text>
                </View>,
              );
            }
          })
          .catch(err => {});
      } else {
        setBody(null);
      }
    }, 700);
    return () => {
      clearTimeout(handler);
    };
  }, [searchText]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <SearchView
        ref={searchRef}
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
        auto
        onSearch={text => {
          setSearchText(text);
        }}
        onSubmit={text => {}}
      />
      {body}
    </View>
  );
};

const mstp = (state: RootState) => ({});

const mdtp = (dispatch: Dispatch) => ({
  search: payload => dispatch.user.search(payload),
});

export default connect(mstp, mdtp)(SearchScreen);
