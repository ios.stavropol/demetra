import React, {useCallback, useEffect, useRef} from 'react';
import {
  BackHandler,
  View,
  Image,
  ActivityIndicator,
  Text,
  ScrollView,
  Dimensions,
} from 'react-native';
import {API, StorageHelper} from './../../services';
import {RootState, Dispatch} from './../../store';
import {connect, useDispatch, useSelector} from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import SearchView from '../../components/SearchView';
import TopButtonView from '../../components/Users/TopButtonView';
import BirthdayView from '../../components/Users/BirthdayView';
import UserView from '../../components/Users/UserView';
import UnitView from '../../components/Users/UnitView';

const UsersScreen = ({
  getUsers,
  setUsers,
  getStructure,
  users,
  structure,
  birthdays,
}) => {
  const navigation = useNavigation();

  const [tab, setTab] = React.useState(0);
  const [body, setBody] = React.useState(null);
  const [search, setSearch] = React.useState('');
  const [objects, setObjects] = React.useState([]);
  const [objects2, setObjects2] = React.useState([]);
  const [view1, setView1] = React.useState(null);
  const [view2, setView2] = React.useState(null);
  const [view3, setView3] = React.useState(null);
  const scroll = useRef(null);

  const [rowCount, setRowCount] = React.useState(10);
  const [rowCount2, setRowCount2] = React.useState(10);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener('hardwareBackPress', function () {
        navigation.goBack();
        return true;
      });
    }, []),
  );

  useEffect(() => {
    StorageHelper.getData('users')
      .then(res => {
        if (res?.length) {
          res = JSON.parse(res);
          if (res) {
            setUsers(res);
            loadUsers(false);
          } else {
            loadUsers(true);
          }
        } else {
          loadUsers(true);
        }
      })
      .catch(err => {
        loadUsers(true);
      });
    StorageHelper.getData('structure')
      .then(res => {
        console.warn('res: ', res);
        loadStructure();
      })
      .catch(err => {
        loadStructure();
      });
  }, []);

  const loadUsers = load => {
    getUsers(load)
      .then(() => {})
      .catch(err => {});
  };

  const loadStructure = () => {
    getStructure()
      .then(() => {})
      .catch(err => {});
  };

  useEffect(() => {
    let array = [];
    for (let i = 0; i < Math.min(rowCount, users?.length); i++) {
      array.push(users[i]);
    }
    setObjects(array);
  }, [rowCount]);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < Math.min(rowCount2, birthdays?.length); i++) {
      array.push(birthdays[i]);
    }
    setObjects2(array);
  }, [rowCount2]);

  useEffect(() => {
    if (tab === 0) {
      if (search.length) {
        let array = users.filter(a => {
          return (
            a.FULL_NAME.toLocaleLowerCase().indexOf(
              search.toLocaleLowerCase(),
            ) !== -1
          );
        });
        setObjects(array);
      } else {
        // setObjects(users);
        let array = [];
        for (let i = 0; i < 10; i++) {
          array.push(users[i]);
        }
        setObjects(array);
      }
    } else if (tab === 1) {
      if (search?.length) {
        let array = birthdays.filter(a => {
          return (
            a.FULL_NAME.toLocaleLowerCase().indexOf(
              search.toLocaleLowerCase(),
            ) !== -1
          );
        });
        setObjects2(array);
      } else {
        // setObjects2(birthdays);
        let array = [];
        for (let i = 0; i < 10; i++) {
          array.push(birthdays[i]);
        }
        setObjects2(array);
      }
    }
  }, [search]);

  useEffect(() => {
    // setObjects(users);
    let array = [];
    for (let i = 0; i < 10; i++) {
      array.push(users[i]);
    }
    setObjects(array);
  }, [users]);

  // useEffect(() => {
  //   setObjects2(birthdays);
  // }, [birthdays]);

  useEffect(() => {
    if (tab === 0) {
      scroll?.current?.scrollTo({x: 0, animated: true});
    } else if (tab === 1) {
      scroll?.current.scrollTo({
        x: Common.getLengthByIPhone7(0),
        animated: true,
      });
    } else if (tab === 2) {
      scroll.current.scrollTo({
        x: 2 * Common.getLengthByIPhone7(0),
        animated: true,
      });
    }
  }, [tab]);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < objects?.length; i++) {
      array.push(
        <UserView
          style={{
            marginTop:
              i === 0
                ? Common.getLengthByIPhone7(20)
                : Common.getLengthByIPhone7(10),
            marginBottom:
              i + 1 === objects?.length ? Common.getLengthByIPhone7(10) : 0,
          }}
          index={i}
          data={objects[i]}
          onClick={() => {
            navigation.navigate('AlienProfile', {data: objects[i]});
          }}
        />,
      );
    }
    setView1(array);
  }, [objects]);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < objects2?.length; i++) {
      array.push(
        <BirthdayView
          style={{
            marginTop:
              i === 0
                ? Common.getLengthByIPhone7(20)
                : Common.getLengthByIPhone7(10),
            marginBottom:
              i + 1 === objects2?.length ? Common.getLengthByIPhone7(10) : 0,
          }}
          index={i}
          data={objects2[i]}
          onClick={() => {
            navigation.navigate('AlienProfile', {data: {ID: objects2[i].ID}});
          }}
        />,
      );
    }
    setView2(array);
  }, [objects2]);

  useEffect(() => {
    let array = [];
    for (let i = 0; i < structure?.length; i++) {
      array.push(
        <UnitView
          style={{
            marginTop:
              i === 0
                ? Common.getLengthByIPhone7(20)
                : Common.getLengthByIPhone7(10),
            marginBottom:
              i + 1 === structure?.length ? Common.getLengthByIPhone7(10) : 0,
          }}
          index={i}
          first
          onChiefClick={obj => {
            navigation.navigate('AlienProfile', {data: obj});
          }}
          onUnitClick={obj => {
            navigation.navigate('Unit', {data: obj});
          }}
          data={structure[i]}
        />,
      );
    }
    setView3(
      <ScrollView
        style={{
          backgroundColor: colors.BACKGROUND_COLOR,
          width: Common.getLengthByIPhone7(0),
          flex: 1,
          //   position: 'absolute',
          //   left: 0,
          //   top: 0,
          //   bottom: 0,
          //   zIndex: tab === 2 ? 100 : 1,
        }}
        contentContainerStyle={{
          alignItems: 'center',
        }}>
        {array}
      </ScrollView>,
    );
  }, [structure]);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.BACKGROUND_COLOR,
        alignItems: 'center',
        justifyContent: 'flex-start',
      }}>
      <TopButtonView
        style={{
          marginTop: Common.getLengthByIPhone7(20),
        }}
        index={tab}
        action={obj => {
          setTab(obj);
          setBody(
            <View
              style={{
                width: Common.getLengthByIPhone7(0),
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <ActivityIndicator />
            </View>,
          );
          setSearch('');
          let array = [];
          for (let i = 0; i < 10; i++) {
            array.push(users[i]);
          }
          setObjects(array);
          array = [];
          for (let i = 0; i < 10; i++) {
            array.push(birthdays[i]);
          }
          setObjects2(array);
        }}
      />
      {tab === 2 ? null : (
        <SearchView
          style={{
            marginTop: Common.getLengthByIPhone7(20),
          }}
          value={search}
          onSearch={text => {
            setSearch(text);
          }}
        />
      )}
      <View
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
          alignItems: 'center',
        }}>
        <ScrollView
          style={{
            width: Common.getLengthByIPhone7(0),
            flex: 1,
          }}
          pagingEnabled
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          ref={el => (scroll.current = el)}
          onMomentumScrollEnd={event => {
            let page = Math.round(
              parseFloat(
                event.nativeEvent.contentOffset.x /
                  Dimensions.get('window').width,
              ),
            );
            // console.warn(page);
            setTab(page);
          }}
          contentContainerStyle={{
            alignItems: 'flex-start',
            justifyContent: 'center',
          }}>
          <ScrollView
            style={{
              backgroundColor: colors.BACKGROUND_COLOR,
              width: Common.getLengthByIPhone7(0),
              flex: 1,
            }}
            contentContainerStyle={{
              alignItems: 'center',
            }}
            onScroll={event => {
              if (
                event.nativeEvent.contentSize.height - 300 <
                event.nativeEvent.layoutMeasurement.height +
                  event.nativeEvent.contentOffset.y
              ) {
                if (search?.length === 0) {
                  let obj = JSON.parse(JSON.stringify(rowCount));
                  obj = obj + 10;
                  setRowCount(obj);
                }
              }
            }}>
            {view1}
          </ScrollView>
          <ScrollView
            style={{
              backgroundColor: colors.BACKGROUND_COLOR,
              width: Common.getLengthByIPhone7(0),
              flex: 1,
            }}
            contentContainerStyle={{
              alignItems: 'center',
            }}
            onScroll={event => {
              if (
                event.nativeEvent.contentSize.height - 300 <
                event.nativeEvent.layoutMeasurement.height +
                  event.nativeEvent.contentOffset.y
              ) {
                if (search?.length === 0) {
                  let obj = JSON.parse(JSON.stringify(rowCount2));
                  obj = obj + 10;
                  setRowCount2(obj);
                }
              }
            }}>
            {view2}
          </ScrollView>
          {view3}
        </ScrollView>
      </View>
    </View>
  );
};

const mstp = (state: RootState) => ({
  users: state.user.users,
  birthdays: state.user.birthdays,
  structure: state.user.structure,
});

const mdtp = (dispatch: Dispatch) => ({
  getUsers: () => dispatch.user.getUsers(),
  getStructure: () => dispatch.user.getStructure(),
  setUsers: payload => dispatch.user.setUsers(payload),
});

export default connect(mstp, mdtp)(UsersScreen);
