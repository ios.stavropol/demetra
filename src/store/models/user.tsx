//@ts-nocheck
import {createModel} from '@rematch/core';
import {API, StorageHelper} from './../../services';
import type {RootModel} from './../models';
import {config} from './../../constants';

type UserState = {
  isRequestGoing: false;
  userProfile: null;
  token: null;
  accessToken: null;
  importants: [];
  news: [];
  birthdays: [];
  users: [];
  structure: [];
  db: [];
  dbRaw: [];
  bearer: null;
  autoFocus: null;
  deviceId: null;
  showAlert: null;
};

const user = createModel<RootModel>()({
  state: {
    isRequestGoing: false,
    userProfile: null,
    importants: [],
    news: [],
    birthdays: [],
    token: null,
    users: [],
    structure: [],
    db: [],
    dbRaw: [],
    accessToken: null,
    bearer: null,
    autoFocus: null,
    deviceId: null,
    showAlert: null,
  } as UserState,
  reducers: {
    setRequestGoingStatus: (state, payload: boolean) => ({
      ...state,
      isRequestGoing: payload,
    }),
    setUserProfile: (state, payload: object) => ({
      ...state,
      userProfile: payload,
    }),
    setAutoFocus: (state, payload: object) => ({
      ...state,
      autoFocus: payload,
    }),
    setBearer: (state, payload: object) => ({
      ...state,
      bearer: payload,
    }),
    setShowAlert: (state, payload: object) => ({
      ...state,
      showAlert: payload,
    }),
    setToken: (state, payload: object) => ({
      ...state,
      token: payload,
    }),
    setDeviceId: (state, payload: object) => ({
      ...state,
      deviceId: payload,
    }),
    setAccessToken: (state, payload: object) => ({
      ...state,
      accessToken: payload,
    }),
    setImportants: (state, payload: object) => ({
      ...state,
      importants: payload,
    }),
    setNews: (state, payload: object) => ({
      ...state,
      news: payload,
    }),
    setUsers: (state, payload: object) => ({
      ...state,
      users: payload,
    }),
    setBirthdays: (state, payload: object) => ({
      ...state,
      birthdays: payload,
    }),
    setStructure: (state, payload: object) => ({
      ...state,
      structure: payload,
    }),
    setDB: (state, payload: object) => ({
      ...state,
      db: payload,
    }),
    setDBRaw: (state, payload: object) => ({
      ...state,
      dbRaw: payload,
    }),
  },
  effects: dispatch => {
    return {
      async login(payload, rootState) {
        return new Promise((resolve, reject) => {
          if (!rootState.user.isRequestGoing) {
            dispatch.user.setRequestGoingStatus(true);
            API.common
              .login(payload)
              .then(response => {
                console.warn('login -> response', response);
                dispatch.user.setRequestGoingStatus(false);
                if (response.status == 200) {
                  API.setToken(response.data.sessid);
                  API.loadToken(response.data.sessid);
                  dispatch.user.setToken(response.data.sessid);
                  dispatch.user.setAccessToken(response.data.sessid);
                  StorageHelper.saveData('url', config.BASE_URL);
                  resolve(response.data);
                } else {
                  reject(response.data.message);
                }
              })
              .catch(err => {
                dispatch.user.setRequestGoingStatus(false);
                console.warn(err.response);
                reject(err.response.data.message);
              });
          }
        });
      },
      async sendCode(payload) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .sendCode(payload)
            .then(response => {
              console.warn('sendCode -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                dispatch.user.setUserProfile(response.data);
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn('sendCode err: ', err.response);
              // if (err.response.status == 400) {
              reject(err.response.data.message);
              // } else {
              // 	reject('Произошла неизвестная ошибка! Повторите снова.');
              // }
            });
        });
      },
      async restorePassword(payload) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .restorePassword(payload)
            .then(response => {
              console.warn('restorePassword -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn('restorePassword err: ', err.response);
              // if (err.response.status == 400) {
              reject(err.response.data.message);
              // } else {
              // 	reject('Произошла неизвестная ошибка! Повторите снова.');
              // }
            });
        });
      },
      async changePassword(payload) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .changePassword(payload)
            .then(response => {
              console.warn('changePassword -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn('changePassword err: ', err.response);
              // if (err.response.status == 400) {
              reject(err.response.data.message);
              // } else {
              // 	reject('Произошла неизвестная ошибка! Повторите снова.');
              // }
            });
        });
      },
      async getProfile(rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .getProfile()
            .then(response => {
              console.warn('getProfile -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                dispatch.user.setUserProfile(response.data);
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn('getProfile err: ', err.response);
              if (err.response.status == 401) {
                if (rootState?.user?.showAlert) {
                  rootState.user.showAlert();
                }
                reject();
              } else {
                reject(err.response.data.message);
              }
              // } else {
              // 	reject('Произошла неизвестная ошибка! Повторите снова.');
              // }
            });
        });
      },
      async getImportant(payload, rootState) {
        return new Promise((resolve, reject) => {
          API.common
            .getImportant(payload.pageNumber, payload.rowCount)
            .then(response => {
              console.warn('getImportant -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                let array = response.data;
                for (let i = 0; i < array.length; i++) {
                  let date = array[i].LOG_DATE;
                  date = date.split(' ');
                  let a = date[0].split('.');
                  a = a[2] + '-' + a[1] + '-' + a[0];
                  date = a + 'T' + date[1];
                  array[i].LOG_DATE = date;
                }
                array = array.sort((a, b) => {
                  return a.LOG_DATE < b.LOG_DATE;
                });
                if (payload.pageNumber == 1) {
                  dispatch.user.setImportants(array);
                } else {
                  let array2 = JSON.parse(
                    JSON.stringify(rootState.user.importants),
                  );
                  array2 = array2.concat(array);
                  dispatch.user.setImportants(array2);
                }
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn(err.response);
              if (err.response.status == 401) {
                if (rootState?.user?.showAlert) {
                  rootState.user.showAlert();
                }
              }
              if (err.response.status == 400) {
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async readImportant(payload, rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .readImportant(payload)
            .then(response => {
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              if (err.response.status == 401) {
                if (rootState?.user?.showAlert) {
                  rootState.user.showAlert();
                }
              }
              if (err.response.status == 400) {
                console.warn(err.response);
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async getNews(payload, rootState) {
        return new Promise((resolve, reject) => {
          API.common
            .getNews(payload.pageNumber, payload.rowCount)
            .then(response => {
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                let array = response.data;
                for (let i = 0; i < array.length; i++) {
                  let date = array[i].LOG_DATE;
                  date = date.split(' ');
                  let a = date[0].split('.');
                  a = a[2] + '-' + a[1] + '-' + a[0];
                  date = a + 'T' + date[1];
                  array[i].LOG_DATE = date;
                }
                array = array.sort((a, b) => {
                  return a.LOG_DATE < b.LOG_DATE;
                });
                if (payload.pageNumber == 1) {
                  dispatch.user.setNews(array);
                } else {
                  let array2 = JSON.parse(JSON.stringify(rootState.user.news));
                  array2 = array2.concat(array);
                  dispatch.user.setNews(array2);
                }
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn(err.response);
              if (err.response.status == 401) {
                if (rootState?.user?.showAlert) {
                  rootState.user.showAlert();
                }
              }
              if (err.response.status == 400) {
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async getBirthdays() {
        return new Promise((resolve, reject) => {
          API.common
            .getBirthdays()
            .then(response => {
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                let array = response.data;
                // for (let i = 0; i < array.length; i++) {
                //   let date = array[i].PERSONAL_BIRTHDAY;
                //   let a = date.split('.');
                //   let birthdayDate = new Date(currentYear, a[1] - 1, a[0]);
                //   a = a[2] + '-' + a[1] + '-' + a[0];
                //   array[i].PERSONAL_BIRTHDAY = a;
                //   let currentYear = new Date().getFullYear();
                //   var now = new Date().valueOf();
                //   if (birthdayDate.valueOf() < now) {
                //     birthdayDate.setFullYear(currentYear + 1);
                //   }
                //   array[i].BIRTHDAY = birthdayDate;
                //   array[i].FROM = birthdayDate.valueOf() - now;
                // }

                // array = array.sort((a, b) => {
                //   return a.FROM > b.FROM;
                // });
                StorageHelper.saveData('birthdays', JSON.stringify(array));
                dispatch.user.setBirthdays(array);
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);

              if (err.response.status == 400) {
                console.warn(err.response);
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async getUsers(load = false) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(load);
          API.common
            .getUsers()
            .then(response => {
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                let array = response.data;
                array = array.sort((a, b) => {
                  a.FULL_NAME > b.FULL_NAME;
                });
                StorageHelper.saveData('users', JSON.stringify(array));
                dispatch.user.setUsers(array);
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              if (err.response.status == 400) {
                console.warn(err.response);
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async getKnowledgebase() {
        return new Promise((resolve, reject) => {
          API.common
            .getKnowledgebase()
            .then(response => {
              if (response.status == 200) {
                let array = response.data;
                dispatch.user.setDB(array);
                dispatch.user.setDBRaw(array);
                resolve(array);
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              console.warn(err.response);
              if (err.response.status == 400) {
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async search(payload, rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .search(payload)
            .then(response => {
              console.warn('search -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve(response.data);
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              if (err.response.status == 401) {
                if (rootState?.user?.showAlert) {
                  rootState.user.showAlert();
                }
              }
              if (err.response.status == 400) {
                console.warn(err.response);
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async getStructure(rootState) {
        return new Promise((resolve, reject) => {
          API.common
            .getStructure()
            .then(response => {
              if (response.status == 200) {
                let array = response.data;
                console.warn('getStructure: ', JSON.stringify(array));
                StorageHelper.saveData('structure', JSON.stringify(array));
                dispatch.user.setStructure(array);
                resolve();
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              console.warn(err.response);
              if (err.response.status == 401) {
                if (rootState?.user.showAlert) {
                  rootState?.user.showAlert();
                }
              }
              if (err.response.status == 400) {
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async getDepartmentEmployee(payload, rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .getDepartmentEmployee(payload)
            .then(response => {
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve(response.data);
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              if (err.response.status == 401) {
                if (rootState?.user.showAlert) {
                  rootState?.user.showAlert();
                }
              }
              if (err.response.status == 400) {
                console.warn(err.response);
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async setImportantNotification(payload, rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .setImportantNotification(payload)
            .then(response => {
              console.warn('setImportantNotification: ', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve(response.data);
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              if (err.response.status == 401) {
                if (rootState?.user.showAlert) {
                  rootState?.user.showAlert();
                }
              }
              if (err.response.status == 400) {
                console.warn(err.response);
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async setNewsNotification(payload, rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .setNewsNotification(payload)
            .then(response => {
              console.warn('setNewsNotification: ', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve(response.data);
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              if (err.response.status == 401) {
                if (rootState?.user.showAlert) {
                  rootState?.user.showAlert();
                }
              }
              if (err.response.status == 400) {
                console.warn(err.response);
                reject(err.response.data.error);
              } else {
                reject('Произошла неизвестная ошибка! Повторите снова.');
              }
            });
        });
      },
      async getUserById(payload, rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .getUserById(payload)
            .then(response => {
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve(response.data);
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn(err.response);
              if (err.response.status == 401) {
                if (rootState?.user.showAlert) {
                  rootState?.user.showAlert();
                }
              }
              reject(err.response.data.message);
            });
        });
      },
      async getNewsById(payload, rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .getNewsById(payload)
            .then(response => {
              console.warn('getNewsById -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve(response.data);
              } else {
                reject(response.data.message);
              }
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn(err.response);
              if (err.response.status == 401) {
                if (rootState?.user.showAlert) {
                  rootState?.user.showAlert();
                }
              }
              reject(err.response.data.message);
            });
        });
      },
      async getImportantById(payload, rootState) {
        return new Promise((resolve, reject) => {
          dispatch.user.setRequestGoingStatus(true);
          API.common
            .getImportantById(payload)
            .then(response => {
              console.warn('getImportantById -> response', response);
              dispatch.user.setRequestGoingStatus(false);
              if (response.status == 200) {
                resolve(response.data);
              } else {
                reject(response.data.message);
              }
              // rootState?.user.showAlert();
            })
            .catch(err => {
              dispatch.user.setRequestGoingStatus(false);
              console.warn(err);
              if (err.response.status == 401) {
                if (rootState?.user.showAlert) {
                  rootState?.user.showAlert();
                }
              }
              reject(err.response.data.message);
            });
        });
      },
    };
  },
});

export default user;
