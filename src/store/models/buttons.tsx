//@ts-nocheck
import {createModel} from '@rematch/core';
import {API, StorageHelper} from './../../services';
import {ErrorsHelper} from './../../helpers';
import {Dispatch} from 'store';
import type {RootModel} from './../models';
import AsyncStorage from '@react-native-async-storage/async-storage';

type ButtonState = {
  changeTab: number;
};

const buttons = createModel<RootModel>()({
  state: {
    changeTab: 0,
  } as ButtonState,
  reducers: {
    setChangeTab: (state, payload: boolean) => ({
      ...state,
      changeTab: payload,
    }),
  },
  effects: dispatch => {
    return {
      // showToastMessage(text) {
      // 	dispatch.buttons.setToastText(text);
      // 	dispatch.buttons.setShowToast(true);
      // },
    };
  },
});

export default buttons;
