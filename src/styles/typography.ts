import Common from "../utilities/Common";
import { colors } from ".";
// FONT FAMILY
export const FONT_FAMILY_REGULAR = 'Poppins-Regular';
export const FONT_FAMILY_BOLD = 'Poppins-Bold';
export const FONT_FAMILY_SEMIBOLD = 'Poppins-SemiBold';

export const H1_M24_140 = {
	color: colors.BLACK_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: '500',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(22),
	lineHeight: Common.getLengthByIPhone7(22)*1.4,
};

export const H2_M20_140 = {
	color: colors.BLACK_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: '500',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(18),
	lineHeight: Common.getLengthByIPhone7(18)*1.4,
};

export const H2_M22_140_LINK = {
	color: colors.BLACK_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: '500',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(20),
	lineHeight: Common.getLengthByIPhone7(20)*1.4,
};

export const H3_M18_140 = {
	color: colors.BLACK_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: '500',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(16),
	lineHeight: Common.getLengthByIPhone7(16)*1.4,
};

export const H4_R16_140 = {
	color: colors.BLACK_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: 'normal',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(14),
	lineHeight: Common.getLengthByIPhone7(14)*1.4,
};

export const H4_R14_140 = {
	color: colors.BLACK_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: 'normal',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(13),
	lineHeight: Common.getLengthByIPhone7(13)*1.4,
};

export const H4_R12_140 = {
	color: colors.BLACK_COLOR,
	fontFamily: FONT_FAMILY_REGULAR,
	fontWeight: 'normal',
	textAlign: 'left',
	fontSize: Common.getLengthByIPhone7(12),
	lineHeight: Common.getLengthByIPhone7(12)*1.4,
};