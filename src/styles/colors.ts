export const TEXT_COLOR = '#4E5C6B';
export const YELLOW_ACTIVE_COLOR = '#F8AA2B';
export const YELLOW_INACTIVE_COLOR = 'rgba(248, 170, 43, 0.4)';
export const BACKGROUND_COLOR = '#ffffff';
export const BORDER_COLOR = '#f2f2f5';
export const GRAY_COLOR = '#C4C4C4';
export const VIEW_COLOR = '#F8F8F8';
export const RED_COLOR = '#F35B2B';
