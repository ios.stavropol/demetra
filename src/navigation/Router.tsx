import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Image, Platform, View, Text} from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {colors} from '../styles';
import {typography} from '../styles';
import SplashScreen from '../screens/Splash';
import LoginScreen from '../screens/Login';
import ForgotPasswordScreen from '../screens/ForgotPassword';
import OTPScreen from '../screens/OTP';
import MainScreen from '../screens/Main';
import SearchScreen from '../screens/Search';
import NewsScreen from '../screens/News';
import NewsPageScreen from '../screens/NewsPage';
import ImportantPageScreen from '../screens/ImportantPage';
import DBScreen from '../screens/DB';
import UsersScreen from '../screens/Users';
import MenuScreen from '../screens/Menu';
import ProfileScreen from '../screens/Profile';
import AlienProfileScreen from '../screens/AlienProfile';
import MenuModalScreen from '../screens/MenuModal';
import SecurityScreen from '../screens/Security';
import UnitScreen from '../screens/Unit';
import BrowserScreen from '../screens/Browser';
import SettingsScreen from '../screens/Settings';
import BirthdaysScreen from '../screens/Birthdays';

import Common from './../utilities/Common';
import MenuButton from '../components/MenuButton';
import CloseButton from '../components/CloseButton';

export const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const LoginStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Login'}
        component={LoginScreen}
        options={{
          title: 'Авторизация',
          headerShown: true,
          animation: 'fade',
          presentation: 'card',
          gestureEnabled: false,
          headerBackVisible: false,
          headerShadowVisible: false,
          headerStyle: {
            backgroundColor: 'white',
          },
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'ForgotPassword'}
        component={ForgotPasswordScreen}
        options={{
          title: 'Запрос пароля',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'OTP'}
        component={OTPScreen}
        options={{
          title: 'Двухфакторная аутентификация',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const MainTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Main'}
        component={MainScreen}
        options={{
          headerShown: false,
          gestureEnabled: false,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
        }}
      />
      <Stack.Screen
        name={'NewsPage2'}
        component={NewsPageScreen}
        options={{
          title: 'Новость',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Birthdays'}
        component={BirthdaysScreen}
        options={{
          title: 'Дни рождения',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'AlienProfile3'}
        component={AlienProfileScreen}
        options={{
          title: 'Профиль',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'ImportantPage2'}
        component={ImportantPageScreen}
        options={{
          title: 'Важное сообщение',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Browser2'}
        component={BrowserScreen}
        options={{
          title: 'База знаний',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Profile2'}
        component={ProfileScreen}
        options={{
          title: 'Профиль',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const SearchTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Search'}
        component={SearchScreen}
        options={{
          title: 'Поиск',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'AlienProfile2'}
        component={AlienProfileScreen}
        options={{
          title: 'Профиль',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'NewsPage3'}
        component={NewsPageScreen}
        options={{
          title: 'Новость',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'ImportantPage3'}
        component={ImportantPageScreen}
        options={{
          title: 'Важное сообщение',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Browser3'}
        component={BrowserScreen}
        options={{
          title: 'База знаний',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const NewsTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'News'}
        component={NewsScreen}
        options={{
          title: 'Лента событий',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'NewsPage'}
        component={NewsPageScreen}
        options={{
          title: 'Новость',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'ImportantPage'}
        component={ImportantPageScreen}
        options={{
          title: 'Важное сообщение',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'AlienProfile4'}
        component={AlienProfileScreen}
        options={{
          title: 'Профиль',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Browser4'}
        component={BrowserScreen}
        options={{
          title: 'База знаний',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const DBTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'DB'}
        component={DBScreen}
        options={{
          title: 'База знаний',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Browser'}
        component={BrowserScreen}
        options={{
          title: 'База знаний',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'AlienProfile5'}
        component={AlienProfileScreen}
        options={{
          title: 'Профиль',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const MenuTab = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Menu'}
        component={MenuScreen}
        options={{
          title: 'Меню',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerRight: () => (
            <MenuButton
              style={{
                marginRight: Common.getLengthByIPhone7(17),
              }}
            />
          ),
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Profile'}
        component={ProfileScreen}
        options={{
          title: 'Профиль',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Users'}
        component={UsersScreen}
        options={{
          title: 'Справочник сотрудников',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'AlienProfile'}
        component={AlienProfileScreen}
        options={{
          title: 'Профиль',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
          },
        }}
      />
      <Stack.Screen
        name={'Unit'}
        component={UnitScreen}
        options={{
          title: 'Сотрудники',
          headerShown: true,
          gestureEnabled: true,
          headerBackTitle: '',
          headerShadowVisible: false,
          headerTintColor: colors.TEXT_COLOR,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'OpenSans-SemiBold',
            fontWeight: '600',
            fontSize: Common.getLengthByIPhone7(16),
            color: colors.TEXT_COLOR,
            maxWidth: 150,
          },
        }}
      />
    </Stack.Navigator>
  );
};

const Tabs = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        lazy: false,
        tabBarStyle: {
          backgroundColor: '#faf9f8',
          borderTopWidth: 0,
          shadowColor: '#CED6E8',
          shadowOffset: {width: 0, height: 2},
          shadowOpacity: 1,
          shadowRadius: 10,
          elevation: 4,
        },
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          let title = '';
          if (route.name === 'MainTab') {
            iconName = require('./../assets/ic-tab1.png');
            title = 'Главная';
          } else if (route.name === 'SearchTab') {
            iconName = require('./../assets/ic-tab2.png');
            title = 'Поиск';
          } else if (route.name === 'NewsTab') {
            iconName = require('./../assets/ic-tab3.png');
            title = 'Лента';
          } else if (route.name === 'DBTab') {
            iconName = require('./../assets/ic-tab4.png');
            title = 'База знаний';
          } else if (route.name === 'MenuTab') {
            iconName = require('./../assets/ic-tab5.png');
            title = 'Меню';
          }

          return (
            <View
              style={{
                alignItems: 'center',
                flex: 1,
              }}>
              <Image
                source={iconName}
                style={{
                  marginTop: 8,
                  resizeMode: 'contain',
                  width: 24,
                  height: 24,
                  tintColor: color,
                }}
              />
              <Text
                style={{
                  marginTop: 3,
                  color: color,
                  fontFamily: 'OpenSans-SemiBold',
                  fontWeight: '600',
                  textAlign: 'center',
                  fontSize: 10,
                }}>
                {title}
              </Text>
            </View>
          );
        },
        tabBarActiveTintColor: colors.YELLOW_ACTIVE_COLOR,
        tabBarInactiveTintColor: colors.GRAY_COLOR,
      })}>
      <Tab.Screen
        name={'MainTab'}
        component={MainTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={'SearchTab'}
        component={SearchTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={'NewsTab'}
        component={NewsTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={'DBTab'}
        component={DBTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
      <Tab.Screen
        name={'MenuTab'}
        component={MenuTab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};

export default Router = () => {
  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <Stack.Navigator>
          <Stack.Screen
            name={'Splash'}
            component={SplashScreen}
            options={{
              headerShown: false,
              gestureEnabled: false,
            }}
          />
          {/* <Stack.Screen
            name={'Login2'}
            component={LoginStack}
            options={{
              animation: 'fade',
              presentation: 'card',
              headerShown: false,
              gestureEnabled: false,
            }}
          /> */}
          <Stack.Screen
            name={'Login'}
            component={LoginScreen}
            options={{
              title: 'Авторизация',
              headerShown: true,
              animation: 'fade',
              presentation: 'card',
              gestureEnabled: false,
              headerBackVisible: false,
              headerShadowVisible: false,
              headerStyle: {
                backgroundColor: 'white',
              },
              headerTitleAlign: 'center',
              headerTitleStyle: {
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: '600',
                fontSize: Common.getLengthByIPhone7(16),
                color: colors.TEXT_COLOR,
              },
            }}
          />
          <Stack.Screen
            name={'ForgotPassword'}
            component={ForgotPasswordScreen}
            options={{
              title: 'Запрос пароля',
              headerShown: true,
              gestureEnabled: true,
              headerBackTitle: '',
              headerShadowVisible: false,
              headerTintColor: colors.TEXT_COLOR,
              headerTitleAlign: 'center',
              headerTitleStyle: {
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: '600',
                fontSize: Common.getLengthByIPhone7(16),
                color: colors.TEXT_COLOR,
              },
            }}
          />
          <Stack.Screen
            name={'OTP'}
            component={OTPScreen}
            options={{
              title: 'Двухфакторная аутентификация',
              headerShown: true,
              gestureEnabled: true,
              headerBackTitle: '',
              headerShadowVisible: false,
              headerTintColor: colors.TEXT_COLOR,
              headerTitleAlign: 'center',
              headerTitleStyle: {
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: '600',
                fontSize: Common.getLengthByIPhone7(16),
                color: colors.TEXT_COLOR,
              },
            }}
          />
          <Stack.Screen
            name={'LoginIn'}
            component={Tabs}
            options={{
              animation: 'fade',
              presentation: 'card',
              headerShown: false,
              gestureEnabled: false,
            }}
          />
          <Stack.Screen
            name={'MenuModal'}
            component={MenuModalScreen}
            options={{
              animation: 'fade',
              presentation: 'card',
              headerShown: false,
              gestureEnabled: false,
              contentStyle: {
                backgroundColor: 'transparent',
              },
              cardStyle: {backgroundColor: 'transparent'},
              presentation: 'transparentModal',
            }}
          />
          <Stack.Screen
            name={'Security'}
            component={SecurityScreen}
            options={({navigation, route}) => ({
              title: 'Безопасность',
              animation: 'slide_from_bottom',
              presentation: 'card',
              headerShown: true,
              gestureEnabled: true,
              headerBackTitle: '',
              headerShadowVisible: false,
              headerBackVisible: false,
              headerTintColor: colors.TEXT_COLOR,
              headerTitleAlign: 'center',
              headerRight: () => (
                <CloseButton
                  navigation={navigation}
                  style={{
                    marginRight: Common.getLengthByIPhone7(17),
                  }}
                />
              ),
              headerTitleStyle: {
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: '600',
                fontSize: Common.getLengthByIPhone7(16),
                color: colors.TEXT_COLOR,
              },
            })}
          />
          <Stack.Screen
            name={'Settings'}
            component={SettingsScreen}
            options={({navigation, route}) => ({
              title: 'Настройки',
              animation: 'slide_from_bottom',
              presentation: 'card',
              headerShown: true,
              gestureEnabled: true,
              headerBackTitle: '',
              headerShadowVisible: false,
              headerBackVisible: false,
              headerTintColor: colors.TEXT_COLOR,
              headerTitleAlign: 'center',
              headerRight: () => (
                <CloseButton
                  navigation={navigation}
                  style={{
                    marginRight: Common.getLengthByIPhone7(17),
                  }}
                />
              ),
              headerTitleStyle: {
                fontFamily: 'OpenSans-SemiBold',
                fontWeight: '600',
                fontSize: Common.getLengthByIPhone7(16),
                color: colors.TEXT_COLOR,
              },
            })}
          />
        </Stack.Navigator>
      </SafeAreaProvider>
    </NavigationContainer>
  );
};
